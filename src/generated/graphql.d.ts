import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  date: any;
  timestamptz: any;
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};

/** columns and relationships of "admins" */
export type Admins = {
   __typename?: 'admins';
  full_name: Scalars['String'];
  id: Scalars['Int'];
  password: Scalars['Int'];
};

/** aggregated selection of "admins" */
export type Admins_Aggregate = {
   __typename?: 'admins_aggregate';
  aggregate?: Maybe<Admins_Aggregate_Fields>;
  nodes: Array<Admins>;
};

/** aggregate fields of "admins" */
export type Admins_Aggregate_Fields = {
   __typename?: 'admins_aggregate_fields';
  avg?: Maybe<Admins_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Admins_Max_Fields>;
  min?: Maybe<Admins_Min_Fields>;
  stddev?: Maybe<Admins_Stddev_Fields>;
  stddev_pop?: Maybe<Admins_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Admins_Stddev_Samp_Fields>;
  sum?: Maybe<Admins_Sum_Fields>;
  var_pop?: Maybe<Admins_Var_Pop_Fields>;
  var_samp?: Maybe<Admins_Var_Samp_Fields>;
  variance?: Maybe<Admins_Variance_Fields>;
};


/** aggregate fields of "admins" */
export type Admins_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Admins_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "admins" */
export type Admins_Aggregate_Order_By = {
  avg?: Maybe<Admins_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Admins_Max_Order_By>;
  min?: Maybe<Admins_Min_Order_By>;
  stddev?: Maybe<Admins_Stddev_Order_By>;
  stddev_pop?: Maybe<Admins_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Admins_Stddev_Samp_Order_By>;
  sum?: Maybe<Admins_Sum_Order_By>;
  var_pop?: Maybe<Admins_Var_Pop_Order_By>;
  var_samp?: Maybe<Admins_Var_Samp_Order_By>;
  variance?: Maybe<Admins_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "admins" */
export type Admins_Arr_Rel_Insert_Input = {
  data: Array<Admins_Insert_Input>;
  on_conflict?: Maybe<Admins_On_Conflict>;
};

/** aggregate avg on columns */
export type Admins_Avg_Fields = {
   __typename?: 'admins_avg_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "admins" */
export type Admins_Avg_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "admins". All fields are combined with a logical 'AND'. */
export type Admins_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Admins_Bool_Exp>>>;
  _not?: Maybe<Admins_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Admins_Bool_Exp>>>;
  full_name?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  password?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "admins" */
export enum Admins_Constraint {
  /** unique or primary key constraint */
  AdminsPkey = 'admins_pkey'
}

/** input type for incrementing integer columne in table "admins" */
export type Admins_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "admins" */
export type Admins_Insert_Input = {
  full_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type Admins_Max_Fields = {
   __typename?: 'admins_max_fields';
  full_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "admins" */
export type Admins_Max_Order_By = {
  full_name?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Admins_Min_Fields = {
   __typename?: 'admins_min_fields';
  full_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "admins" */
export type Admins_Min_Order_By = {
  full_name?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** response of any mutation on the table "admins" */
export type Admins_Mutation_Response = {
   __typename?: 'admins_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Admins>;
};

/** input type for inserting object relation for remote table "admins" */
export type Admins_Obj_Rel_Insert_Input = {
  data: Admins_Insert_Input;
  on_conflict?: Maybe<Admins_On_Conflict>;
};

/** on conflict condition type for table "admins" */
export type Admins_On_Conflict = {
  constraint: Admins_Constraint;
  update_columns: Array<Admins_Update_Column>;
  where?: Maybe<Admins_Bool_Exp>;
};

/** ordering options when selecting data from "admins" */
export type Admins_Order_By = {
  full_name?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** select columns of table "admins" */
export enum Admins_Select_Column {
  /** column name */
  FullName = 'full_name',
  /** column name */
  Id = 'id',
  /** column name */
  Password = 'password'
}

/** input type for updating data in table "admins" */
export type Admins_Set_Input = {
  full_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type Admins_Stddev_Fields = {
   __typename?: 'admins_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "admins" */
export type Admins_Stddev_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Admins_Stddev_Pop_Fields = {
   __typename?: 'admins_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "admins" */
export type Admins_Stddev_Pop_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Admins_Stddev_Samp_Fields = {
   __typename?: 'admins_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "admins" */
export type Admins_Stddev_Samp_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Admins_Sum_Fields = {
   __typename?: 'admins_sum_fields';
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "admins" */
export type Admins_Sum_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** update columns of table "admins" */
export enum Admins_Update_Column {
  /** column name */
  FullName = 'full_name',
  /** column name */
  Id = 'id',
  /** column name */
  Password = 'password'
}

/** aggregate var_pop on columns */
export type Admins_Var_Pop_Fields = {
   __typename?: 'admins_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "admins" */
export type Admins_Var_Pop_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Admins_Var_Samp_Fields = {
   __typename?: 'admins_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "admins" */
export type Admins_Var_Samp_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Admins_Variance_Fields = {
   __typename?: 'admins_variance_fields';
  id?: Maybe<Scalars['Float']>;
  password?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "admins" */
export type Admins_Variance_Order_By = {
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** columns and relationships of "authors" */
export type Authors = {
   __typename?: 'authors';
  birthdate?: Maybe<Scalars['date']>;
  created_at: Scalars['timestamptz'];
  deleted_at?: Maybe<Scalars['timestamptz']>;
  id: Scalars['Int'];
  name: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  updated_at: Scalars['timestamptz'];
};

/** aggregated selection of "authors" */
export type Authors_Aggregate = {
   __typename?: 'authors_aggregate';
  aggregate?: Maybe<Authors_Aggregate_Fields>;
  nodes: Array<Authors>;
};

/** aggregate fields of "authors" */
export type Authors_Aggregate_Fields = {
   __typename?: 'authors_aggregate_fields';
  avg?: Maybe<Authors_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Authors_Max_Fields>;
  min?: Maybe<Authors_Min_Fields>;
  stddev?: Maybe<Authors_Stddev_Fields>;
  stddev_pop?: Maybe<Authors_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Authors_Stddev_Samp_Fields>;
  sum?: Maybe<Authors_Sum_Fields>;
  var_pop?: Maybe<Authors_Var_Pop_Fields>;
  var_samp?: Maybe<Authors_Var_Samp_Fields>;
  variance?: Maybe<Authors_Variance_Fields>;
};


/** aggregate fields of "authors" */
export type Authors_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Authors_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "authors" */
export type Authors_Aggregate_Order_By = {
  avg?: Maybe<Authors_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Authors_Max_Order_By>;
  min?: Maybe<Authors_Min_Order_By>;
  stddev?: Maybe<Authors_Stddev_Order_By>;
  stddev_pop?: Maybe<Authors_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Authors_Stddev_Samp_Order_By>;
  sum?: Maybe<Authors_Sum_Order_By>;
  var_pop?: Maybe<Authors_Var_Pop_Order_By>;
  var_samp?: Maybe<Authors_Var_Samp_Order_By>;
  variance?: Maybe<Authors_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "authors" */
export type Authors_Arr_Rel_Insert_Input = {
  data: Array<Authors_Insert_Input>;
  on_conflict?: Maybe<Authors_On_Conflict>;
};

/** aggregate avg on columns */
export type Authors_Avg_Fields = {
   __typename?: 'authors_avg_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "authors" */
export type Authors_Avg_Order_By = {
  id?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "authors". All fields are combined with a logical 'AND'. */
export type Authors_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Authors_Bool_Exp>>>;
  _not?: Maybe<Authors_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Authors_Bool_Exp>>>;
  birthdate?: Maybe<Date_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  deleted_at?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  password?: Maybe<String_Comparison_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "authors" */
export enum Authors_Constraint {
  /** unique or primary key constraint */
  AuthorsPkey = 'authors_pkey'
}

/** input type for incrementing integer columne in table "authors" */
export type Authors_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "authors" */
export type Authors_Insert_Input = {
  birthdate?: Maybe<Scalars['date']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Authors_Max_Fields = {
   __typename?: 'authors_max_fields';
  birthdate?: Maybe<Scalars['date']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "authors" */
export type Authors_Max_Order_By = {
  birthdate?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Authors_Min_Fields = {
   __typename?: 'authors_min_fields';
  birthdate?: Maybe<Scalars['date']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "authors" */
export type Authors_Min_Order_By = {
  birthdate?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "authors" */
export type Authors_Mutation_Response = {
   __typename?: 'authors_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Authors>;
};

/** input type for inserting object relation for remote table "authors" */
export type Authors_Obj_Rel_Insert_Input = {
  data: Authors_Insert_Input;
  on_conflict?: Maybe<Authors_On_Conflict>;
};

/** on conflict condition type for table "authors" */
export type Authors_On_Conflict = {
  constraint: Authors_Constraint;
  update_columns: Array<Authors_Update_Column>;
  where?: Maybe<Authors_Bool_Exp>;
};

/** ordering options when selecting data from "authors" */
export type Authors_Order_By = {
  birthdate?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** select columns of table "authors" */
export enum Authors_Select_Column {
  /** column name */
  Birthdate = 'birthdate',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Password = 'password',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "authors" */
export type Authors_Set_Input = {
  birthdate?: Maybe<Scalars['date']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate stddev on columns */
export type Authors_Stddev_Fields = {
   __typename?: 'authors_stddev_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "authors" */
export type Authors_Stddev_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Authors_Stddev_Pop_Fields = {
   __typename?: 'authors_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "authors" */
export type Authors_Stddev_Pop_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Authors_Stddev_Samp_Fields = {
   __typename?: 'authors_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "authors" */
export type Authors_Stddev_Samp_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Authors_Sum_Fields = {
   __typename?: 'authors_sum_fields';
  id?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "authors" */
export type Authors_Sum_Order_By = {
  id?: Maybe<Order_By>;
};

/** update columns of table "authors" */
export enum Authors_Update_Column {
  /** column name */
  Birthdate = 'birthdate',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Password = 'password',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Authors_Var_Pop_Fields = {
   __typename?: 'authors_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "authors" */
export type Authors_Var_Pop_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Authors_Var_Samp_Fields = {
   __typename?: 'authors_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "authors" */
export type Authors_Var_Samp_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Authors_Variance_Fields = {
   __typename?: 'authors_variance_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "authors" */
export type Authors_Variance_Order_By = {
  id?: Maybe<Order_By>;
};

/** columns and relationships of "comments" */
export type Comments = {
   __typename?: 'comments';
  comment: Scalars['String'];
  created_at: Scalars['timestamptz'];
  id: Scalars['Int'];
  news_id: Scalars['Int'];
};

/** aggregated selection of "comments" */
export type Comments_Aggregate = {
   __typename?: 'comments_aggregate';
  aggregate?: Maybe<Comments_Aggregate_Fields>;
  nodes: Array<Comments>;
};

/** aggregate fields of "comments" */
export type Comments_Aggregate_Fields = {
   __typename?: 'comments_aggregate_fields';
  avg?: Maybe<Comments_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Comments_Max_Fields>;
  min?: Maybe<Comments_Min_Fields>;
  stddev?: Maybe<Comments_Stddev_Fields>;
  stddev_pop?: Maybe<Comments_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Comments_Stddev_Samp_Fields>;
  sum?: Maybe<Comments_Sum_Fields>;
  var_pop?: Maybe<Comments_Var_Pop_Fields>;
  var_samp?: Maybe<Comments_Var_Samp_Fields>;
  variance?: Maybe<Comments_Variance_Fields>;
};


/** aggregate fields of "comments" */
export type Comments_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Comments_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "comments" */
export type Comments_Aggregate_Order_By = {
  avg?: Maybe<Comments_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Comments_Max_Order_By>;
  min?: Maybe<Comments_Min_Order_By>;
  stddev?: Maybe<Comments_Stddev_Order_By>;
  stddev_pop?: Maybe<Comments_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Comments_Stddev_Samp_Order_By>;
  sum?: Maybe<Comments_Sum_Order_By>;
  var_pop?: Maybe<Comments_Var_Pop_Order_By>;
  var_samp?: Maybe<Comments_Var_Samp_Order_By>;
  variance?: Maybe<Comments_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "comments" */
export type Comments_Arr_Rel_Insert_Input = {
  data: Array<Comments_Insert_Input>;
  on_conflict?: Maybe<Comments_On_Conflict>;
};

/** aggregate avg on columns */
export type Comments_Avg_Fields = {
   __typename?: 'comments_avg_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "comments" */
export type Comments_Avg_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "comments". All fields are combined with a logical 'AND'. */
export type Comments_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Comments_Bool_Exp>>>;
  _not?: Maybe<Comments_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Comments_Bool_Exp>>>;
  comment?: Maybe<String_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  news_id?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "comments" */
export enum Comments_Constraint {
  /** unique or primary key constraint */
  CommentsPkey = 'comments_pkey'
}

/** input type for incrementing integer columne in table "comments" */
export type Comments_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "comments" */
export type Comments_Insert_Input = {
  comment?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type Comments_Max_Fields = {
   __typename?: 'comments_max_fields';
  comment?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "comments" */
export type Comments_Max_Order_By = {
  comment?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Comments_Min_Fields = {
   __typename?: 'comments_min_fields';
  comment?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "comments" */
export type Comments_Min_Order_By = {
  comment?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "comments" */
export type Comments_Mutation_Response = {
   __typename?: 'comments_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Comments>;
};

/** input type for inserting object relation for remote table "comments" */
export type Comments_Obj_Rel_Insert_Input = {
  data: Comments_Insert_Input;
  on_conflict?: Maybe<Comments_On_Conflict>;
};

/** on conflict condition type for table "comments" */
export type Comments_On_Conflict = {
  constraint: Comments_Constraint;
  update_columns: Array<Comments_Update_Column>;
  where?: Maybe<Comments_Bool_Exp>;
};

/** ordering options when selecting data from "comments" */
export type Comments_Order_By = {
  comment?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** select columns of table "comments" */
export enum Comments_Select_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  NewsId = 'news_id'
}

/** input type for updating data in table "comments" */
export type Comments_Set_Input = {
  comment?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type Comments_Stddev_Fields = {
   __typename?: 'comments_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "comments" */
export type Comments_Stddev_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Comments_Stddev_Pop_Fields = {
   __typename?: 'comments_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "comments" */
export type Comments_Stddev_Pop_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Comments_Stddev_Samp_Fields = {
   __typename?: 'comments_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "comments" */
export type Comments_Stddev_Samp_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Comments_Sum_Fields = {
   __typename?: 'comments_sum_fields';
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "comments" */
export type Comments_Sum_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** update columns of table "comments" */
export enum Comments_Update_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  NewsId = 'news_id'
}

/** aggregate var_pop on columns */
export type Comments_Var_Pop_Fields = {
   __typename?: 'comments_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "comments" */
export type Comments_Var_Pop_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Comments_Var_Samp_Fields = {
   __typename?: 'comments_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "comments" */
export type Comments_Var_Samp_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Comments_Variance_Fields = {
   __typename?: 'comments_variance_fields';
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "comments" */
export type Comments_Variance_Order_By = {
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};


/** expression to compare columns of type date. All fields are combined with logical 'AND'. */
export type Date_Comparison_Exp = {
  _eq?: Maybe<Scalars['date']>;
  _gt?: Maybe<Scalars['date']>;
  _gte?: Maybe<Scalars['date']>;
  _in?: Maybe<Array<Scalars['date']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['date']>;
  _lte?: Maybe<Scalars['date']>;
  _neq?: Maybe<Scalars['date']>;
  _nin?: Maybe<Array<Scalars['date']>>;
};

/** mutation root */
export type Mutation_Root = {
   __typename?: 'mutation_root';
  /** delete data from the table: "admins" */
  delete_admins?: Maybe<Admins_Mutation_Response>;
  /** delete data from the table: "authors" */
  delete_authors?: Maybe<Authors_Mutation_Response>;
  /** delete data from the table: "comments" */
  delete_comments?: Maybe<Comments_Mutation_Response>;
  /** delete data from the table: "news" */
  delete_news?: Maybe<News_Mutation_Response>;
  /** delete data from the table: "reactions" */
  delete_reactions?: Maybe<Reactions_Mutation_Response>;
  /** insert data into the table: "admins" */
  insert_admins?: Maybe<Admins_Mutation_Response>;
  /** insert data into the table: "authors" */
  insert_authors?: Maybe<Authors_Mutation_Response>;
  /** insert data into the table: "comments" */
  insert_comments?: Maybe<Comments_Mutation_Response>;
  /** insert data into the table: "news" */
  insert_news?: Maybe<News_Mutation_Response>;
  /** insert data into the table: "reactions" */
  insert_reactions?: Maybe<Reactions_Mutation_Response>;
  /** update data of the table: "admins" */
  update_admins?: Maybe<Admins_Mutation_Response>;
  /** update data of the table: "authors" */
  update_authors?: Maybe<Authors_Mutation_Response>;
  /** update data of the table: "comments" */
  update_comments?: Maybe<Comments_Mutation_Response>;
  /** update data of the table: "news" */
  update_news?: Maybe<News_Mutation_Response>;
  /** update data of the table: "reactions" */
  update_reactions?: Maybe<Reactions_Mutation_Response>;
};


/** mutation root */
export type Mutation_RootDelete_AdminsArgs = {
  where: Admins_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_AuthorsArgs = {
  where: Authors_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_CommentsArgs = {
  where: Comments_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_NewsArgs = {
  where: News_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_ReactionsArgs = {
  where: Reactions_Bool_Exp;
};


/** mutation root */
export type Mutation_RootInsert_AdminsArgs = {
  objects: Array<Admins_Insert_Input>;
  on_conflict?: Maybe<Admins_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_AuthorsArgs = {
  objects: Array<Authors_Insert_Input>;
  on_conflict?: Maybe<Authors_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CommentsArgs = {
  objects: Array<Comments_Insert_Input>;
  on_conflict?: Maybe<Comments_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_NewsArgs = {
  objects: Array<News_Insert_Input>;
  on_conflict?: Maybe<News_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ReactionsArgs = {
  objects: Array<Reactions_Insert_Input>;
  on_conflict?: Maybe<Reactions_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_AdminsArgs = {
  _inc?: Maybe<Admins_Inc_Input>;
  _set?: Maybe<Admins_Set_Input>;
  where: Admins_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_AuthorsArgs = {
  _inc?: Maybe<Authors_Inc_Input>;
  _set?: Maybe<Authors_Set_Input>;
  where: Authors_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_CommentsArgs = {
  _inc?: Maybe<Comments_Inc_Input>;
  _set?: Maybe<Comments_Set_Input>;
  where: Comments_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_NewsArgs = {
  _inc?: Maybe<News_Inc_Input>;
  _set?: Maybe<News_Set_Input>;
  where: News_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_ReactionsArgs = {
  _inc?: Maybe<Reactions_Inc_Input>;
  _set?: Maybe<Reactions_Set_Input>;
  where: Reactions_Bool_Exp;
};

/** columns and relationships of "news" */
export type News = {
   __typename?: 'news';
  author_id: Scalars['Int'];
  created_at: Scalars['timestamptz'];
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description: Scalars['String'];
  id: Scalars['Int'];
  img_url?: Maybe<Scalars['String']>;
  news_detail: Scalars['String'];
  title: Scalars['String'];
  type: Scalars['String'];
  updated_at: Scalars['timestamptz'];
  views?: Maybe<Scalars['Int']>;
};

/** aggregated selection of "news" */
export type News_Aggregate = {
   __typename?: 'news_aggregate';
  aggregate?: Maybe<News_Aggregate_Fields>;
  nodes: Array<News>;
};

/** aggregate fields of "news" */
export type News_Aggregate_Fields = {
   __typename?: 'news_aggregate_fields';
  avg?: Maybe<News_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<News_Max_Fields>;
  min?: Maybe<News_Min_Fields>;
  stddev?: Maybe<News_Stddev_Fields>;
  stddev_pop?: Maybe<News_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<News_Stddev_Samp_Fields>;
  sum?: Maybe<News_Sum_Fields>;
  var_pop?: Maybe<News_Var_Pop_Fields>;
  var_samp?: Maybe<News_Var_Samp_Fields>;
  variance?: Maybe<News_Variance_Fields>;
};


/** aggregate fields of "news" */
export type News_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<News_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "news" */
export type News_Aggregate_Order_By = {
  avg?: Maybe<News_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<News_Max_Order_By>;
  min?: Maybe<News_Min_Order_By>;
  stddev?: Maybe<News_Stddev_Order_By>;
  stddev_pop?: Maybe<News_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<News_Stddev_Samp_Order_By>;
  sum?: Maybe<News_Sum_Order_By>;
  var_pop?: Maybe<News_Var_Pop_Order_By>;
  var_samp?: Maybe<News_Var_Samp_Order_By>;
  variance?: Maybe<News_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "news" */
export type News_Arr_Rel_Insert_Input = {
  data: Array<News_Insert_Input>;
  on_conflict?: Maybe<News_On_Conflict>;
};

/** aggregate avg on columns */
export type News_Avg_Fields = {
   __typename?: 'news_avg_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "news" */
export type News_Avg_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "news". All fields are combined with a logical 'AND'. */
export type News_Bool_Exp = {
  _and?: Maybe<Array<Maybe<News_Bool_Exp>>>;
  _not?: Maybe<News_Bool_Exp>;
  _or?: Maybe<Array<Maybe<News_Bool_Exp>>>;
  author_id?: Maybe<Int_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  deleted_at?: Maybe<Timestamptz_Comparison_Exp>;
  description?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  img_url?: Maybe<String_Comparison_Exp>;
  news_detail?: Maybe<String_Comparison_Exp>;
  title?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
  views?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "news" */
export enum News_Constraint {
  /** unique or primary key constraint */
  NewsPkey = 'news_pkey'
}

/** input type for incrementing integer columne in table "news" */
export type News_Inc_Input = {
  author_id?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  views?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "news" */
export type News_Insert_Input = {
  author_id?: Maybe<Scalars['Int']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  views?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type News_Max_Fields = {
   __typename?: 'news_max_fields';
  author_id?: Maybe<Scalars['Int']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "news" */
export type News_Max_Order_By = {
  author_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  news_detail?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type News_Min_Fields = {
   __typename?: 'news_min_fields';
  author_id?: Maybe<Scalars['Int']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "news" */
export type News_Min_Order_By = {
  author_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  news_detail?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** response of any mutation on the table "news" */
export type News_Mutation_Response = {
   __typename?: 'news_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<News>;
};

/** input type for inserting object relation for remote table "news" */
export type News_Obj_Rel_Insert_Input = {
  data: News_Insert_Input;
  on_conflict?: Maybe<News_On_Conflict>;
};

/** on conflict condition type for table "news" */
export type News_On_Conflict = {
  constraint: News_Constraint;
  update_columns: Array<News_Update_Column>;
  where?: Maybe<News_Bool_Exp>;
};

/** ordering options when selecting data from "news" */
export type News_Order_By = {
  author_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  news_detail?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** select columns of table "news" */
export enum News_Select_Column {
  /** column name */
  AuthorId = 'author_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  ImgUrl = 'img_url',
  /** column name */
  NewsDetail = 'news_detail',
  /** column name */
  Title = 'title',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Views = 'views'
}

/** input type for updating data in table "news" */
export type News_Set_Input = {
  author_id?: Maybe<Scalars['Int']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  deleted_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  views?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type News_Stddev_Fields = {
   __typename?: 'news_stddev_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "news" */
export type News_Stddev_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type News_Stddev_Pop_Fields = {
   __typename?: 'news_stddev_pop_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "news" */
export type News_Stddev_Pop_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type News_Stddev_Samp_Fields = {
   __typename?: 'news_stddev_samp_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "news" */
export type News_Stddev_Samp_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type News_Sum_Fields = {
   __typename?: 'news_sum_fields';
  author_id?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "news" */
export type News_Sum_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** update columns of table "news" */
export enum News_Update_Column {
  /** column name */
  AuthorId = 'author_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  ImgUrl = 'img_url',
  /** column name */
  NewsDetail = 'news_detail',
  /** column name */
  Title = 'title',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Views = 'views'
}

/** aggregate var_pop on columns */
export type News_Var_Pop_Fields = {
   __typename?: 'news_var_pop_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "news" */
export type News_Var_Pop_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type News_Var_Samp_Fields = {
   __typename?: 'news_var_samp_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "news" */
export type News_Var_Samp_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type News_Variance_Fields = {
   __typename?: 'news_variance_fields';
  author_id?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "news" */
export type News_Variance_Order_By = {
  author_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** query root */
export type Query_Root = {
   __typename?: 'query_root';
  /** fetch data from the table: "admins" */
  admins: Array<Admins>;
  /** fetch aggregated fields from the table: "admins" */
  admins_aggregate: Admins_Aggregate;
  /** fetch data from the table: "admins" using primary key columns */
  admins_by_pk?: Maybe<Admins>;
  /** fetch data from the table: "authors" */
  authors: Array<Authors>;
  /** fetch aggregated fields from the table: "authors" */
  authors_aggregate: Authors_Aggregate;
  /** fetch data from the table: "authors" using primary key columns */
  authors_by_pk?: Maybe<Authors>;
  /** fetch data from the table: "comments" */
  comments: Array<Comments>;
  /** fetch aggregated fields from the table: "comments" */
  comments_aggregate: Comments_Aggregate;
  /** fetch data from the table: "comments" using primary key columns */
  comments_by_pk?: Maybe<Comments>;
  /** fetch data from the table: "news" */
  news: Array<News>;
  /** fetch aggregated fields from the table: "news" */
  news_aggregate: News_Aggregate;
  /** fetch data from the table: "news" using primary key columns */
  news_by_pk?: Maybe<News>;
  /** fetch data from the table: "reactions" */
  reactions: Array<Reactions>;
  /** fetch aggregated fields from the table: "reactions" */
  reactions_aggregate: Reactions_Aggregate;
  /** fetch data from the table: "reactions" using primary key columns */
  reactions_by_pk?: Maybe<Reactions>;
  /** fetch data from the table: "vw_detail_news" */
  vw_detail_news: Array<Vw_Detail_News>;
  /** fetch aggregated fields from the table: "vw_detail_news" */
  vw_detail_news_aggregate: Vw_Detail_News_Aggregate;
  /** fetch data from the table: "vw_news_by_week" */
  vw_news_by_week: Array<Vw_News_By_Week>;
  /** fetch aggregated fields from the table: "vw_news_by_week" */
  vw_news_by_week_aggregate: Vw_News_By_Week_Aggregate;
  /** fetch data from the table: "vw_type_name" */
  vw_type_name: Array<Vw_Type_Name>;
  /** fetch aggregated fields from the table: "vw_type_name" */
  vw_type_name_aggregate: Vw_Type_Name_Aggregate;
};


/** query root */
export type Query_RootAdminsArgs = {
  distinct_on?: Maybe<Array<Admins_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Admins_Order_By>>;
  where?: Maybe<Admins_Bool_Exp>;
};


/** query root */
export type Query_RootAdmins_AggregateArgs = {
  distinct_on?: Maybe<Array<Admins_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Admins_Order_By>>;
  where?: Maybe<Admins_Bool_Exp>;
};


/** query root */
export type Query_RootAdmins_By_PkArgs = {
  id: Scalars['Int'];
};


/** query root */
export type Query_RootAuthorsArgs = {
  distinct_on?: Maybe<Array<Authors_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Authors_Order_By>>;
  where?: Maybe<Authors_Bool_Exp>;
};


/** query root */
export type Query_RootAuthors_AggregateArgs = {
  distinct_on?: Maybe<Array<Authors_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Authors_Order_By>>;
  where?: Maybe<Authors_Bool_Exp>;
};


/** query root */
export type Query_RootAuthors_By_PkArgs = {
  id: Scalars['Int'];
};


/** query root */
export type Query_RootCommentsArgs = {
  distinct_on?: Maybe<Array<Comments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Comments_Order_By>>;
  where?: Maybe<Comments_Bool_Exp>;
};


/** query root */
export type Query_RootComments_AggregateArgs = {
  distinct_on?: Maybe<Array<Comments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Comments_Order_By>>;
  where?: Maybe<Comments_Bool_Exp>;
};


/** query root */
export type Query_RootComments_By_PkArgs = {
  id: Scalars['Int'];
};


/** query root */
export type Query_RootNewsArgs = {
  distinct_on?: Maybe<Array<News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<News_Order_By>>;
  where?: Maybe<News_Bool_Exp>;
};


/** query root */
export type Query_RootNews_AggregateArgs = {
  distinct_on?: Maybe<Array<News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<News_Order_By>>;
  where?: Maybe<News_Bool_Exp>;
};


/** query root */
export type Query_RootNews_By_PkArgs = {
  id: Scalars['Int'];
};


/** query root */
export type Query_RootReactionsArgs = {
  distinct_on?: Maybe<Array<Reactions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Reactions_Order_By>>;
  where?: Maybe<Reactions_Bool_Exp>;
};


/** query root */
export type Query_RootReactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Reactions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Reactions_Order_By>>;
  where?: Maybe<Reactions_Bool_Exp>;
};


/** query root */
export type Query_RootReactions_By_PkArgs = {
  id: Scalars['Int'];
};


/** query root */
export type Query_RootVw_Detail_NewsArgs = {
  distinct_on?: Maybe<Array<Vw_Detail_News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Detail_News_Order_By>>;
  where?: Maybe<Vw_Detail_News_Bool_Exp>;
};


/** query root */
export type Query_RootVw_Detail_News_AggregateArgs = {
  distinct_on?: Maybe<Array<Vw_Detail_News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Detail_News_Order_By>>;
  where?: Maybe<Vw_Detail_News_Bool_Exp>;
};


/** query root */
export type Query_RootVw_News_By_WeekArgs = {
  distinct_on?: Maybe<Array<Vw_News_By_Week_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_News_By_Week_Order_By>>;
  where?: Maybe<Vw_News_By_Week_Bool_Exp>;
};


/** query root */
export type Query_RootVw_News_By_Week_AggregateArgs = {
  distinct_on?: Maybe<Array<Vw_News_By_Week_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_News_By_Week_Order_By>>;
  where?: Maybe<Vw_News_By_Week_Bool_Exp>;
};


/** query root */
export type Query_RootVw_Type_NameArgs = {
  distinct_on?: Maybe<Array<Vw_Type_Name_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Type_Name_Order_By>>;
  where?: Maybe<Vw_Type_Name_Bool_Exp>;
};


/** query root */
export type Query_RootVw_Type_Name_AggregateArgs = {
  distinct_on?: Maybe<Array<Vw_Type_Name_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Type_Name_Order_By>>;
  where?: Maybe<Vw_Type_Name_Bool_Exp>;
};

/** columns and relationships of "reactions" */
export type Reactions = {
   __typename?: 'reactions';
  count: Scalars['Int'];
  id: Scalars['Int'];
  news_id: Scalars['Int'];
};

/** aggregated selection of "reactions" */
export type Reactions_Aggregate = {
   __typename?: 'reactions_aggregate';
  aggregate?: Maybe<Reactions_Aggregate_Fields>;
  nodes: Array<Reactions>;
};

/** aggregate fields of "reactions" */
export type Reactions_Aggregate_Fields = {
   __typename?: 'reactions_aggregate_fields';
  avg?: Maybe<Reactions_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Reactions_Max_Fields>;
  min?: Maybe<Reactions_Min_Fields>;
  stddev?: Maybe<Reactions_Stddev_Fields>;
  stddev_pop?: Maybe<Reactions_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Reactions_Stddev_Samp_Fields>;
  sum?: Maybe<Reactions_Sum_Fields>;
  var_pop?: Maybe<Reactions_Var_Pop_Fields>;
  var_samp?: Maybe<Reactions_Var_Samp_Fields>;
  variance?: Maybe<Reactions_Variance_Fields>;
};


/** aggregate fields of "reactions" */
export type Reactions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Reactions_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "reactions" */
export type Reactions_Aggregate_Order_By = {
  avg?: Maybe<Reactions_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Reactions_Max_Order_By>;
  min?: Maybe<Reactions_Min_Order_By>;
  stddev?: Maybe<Reactions_Stddev_Order_By>;
  stddev_pop?: Maybe<Reactions_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Reactions_Stddev_Samp_Order_By>;
  sum?: Maybe<Reactions_Sum_Order_By>;
  var_pop?: Maybe<Reactions_Var_Pop_Order_By>;
  var_samp?: Maybe<Reactions_Var_Samp_Order_By>;
  variance?: Maybe<Reactions_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "reactions" */
export type Reactions_Arr_Rel_Insert_Input = {
  data: Array<Reactions_Insert_Input>;
  on_conflict?: Maybe<Reactions_On_Conflict>;
};

/** aggregate avg on columns */
export type Reactions_Avg_Fields = {
   __typename?: 'reactions_avg_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "reactions" */
export type Reactions_Avg_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "reactions". All fields are combined with a logical 'AND'. */
export type Reactions_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Reactions_Bool_Exp>>>;
  _not?: Maybe<Reactions_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Reactions_Bool_Exp>>>;
  count?: Maybe<Int_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  news_id?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "reactions" */
export enum Reactions_Constraint {
  /** unique or primary key constraint */
  ReactionsPkey = 'reactions_pkey'
}

/** input type for incrementing integer columne in table "reactions" */
export type Reactions_Inc_Input = {
  count?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "reactions" */
export type Reactions_Insert_Input = {
  count?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type Reactions_Max_Fields = {
   __typename?: 'reactions_max_fields';
  count?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "reactions" */
export type Reactions_Max_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Reactions_Min_Fields = {
   __typename?: 'reactions_min_fields';
  count?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "reactions" */
export type Reactions_Min_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "reactions" */
export type Reactions_Mutation_Response = {
   __typename?: 'reactions_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Reactions>;
};

/** input type for inserting object relation for remote table "reactions" */
export type Reactions_Obj_Rel_Insert_Input = {
  data: Reactions_Insert_Input;
  on_conflict?: Maybe<Reactions_On_Conflict>;
};

/** on conflict condition type for table "reactions" */
export type Reactions_On_Conflict = {
  constraint: Reactions_Constraint;
  update_columns: Array<Reactions_Update_Column>;
  where?: Maybe<Reactions_Bool_Exp>;
};

/** ordering options when selecting data from "reactions" */
export type Reactions_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** select columns of table "reactions" */
export enum Reactions_Select_Column {
  /** column name */
  Count = 'count',
  /** column name */
  Id = 'id',
  /** column name */
  NewsId = 'news_id'
}

/** input type for updating data in table "reactions" */
export type Reactions_Set_Input = {
  count?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type Reactions_Stddev_Fields = {
   __typename?: 'reactions_stddev_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "reactions" */
export type Reactions_Stddev_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Reactions_Stddev_Pop_Fields = {
   __typename?: 'reactions_stddev_pop_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "reactions" */
export type Reactions_Stddev_Pop_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Reactions_Stddev_Samp_Fields = {
   __typename?: 'reactions_stddev_samp_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "reactions" */
export type Reactions_Stddev_Samp_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Reactions_Sum_Fields = {
   __typename?: 'reactions_sum_fields';
  count?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  news_id?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "reactions" */
export type Reactions_Sum_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** update columns of table "reactions" */
export enum Reactions_Update_Column {
  /** column name */
  Count = 'count',
  /** column name */
  Id = 'id',
  /** column name */
  NewsId = 'news_id'
}

/** aggregate var_pop on columns */
export type Reactions_Var_Pop_Fields = {
   __typename?: 'reactions_var_pop_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "reactions" */
export type Reactions_Var_Pop_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Reactions_Var_Samp_Fields = {
   __typename?: 'reactions_var_samp_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "reactions" */
export type Reactions_Var_Samp_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Reactions_Variance_Fields = {
   __typename?: 'reactions_variance_fields';
  count?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  news_id?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "reactions" */
export type Reactions_Variance_Order_By = {
  count?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  news_id?: Maybe<Order_By>;
};

/** subscription root */
export type Subscription_Root = {
   __typename?: 'subscription_root';
  /** fetch data from the table: "admins" */
  admins: Array<Admins>;
  /** fetch aggregated fields from the table: "admins" */
  admins_aggregate: Admins_Aggregate;
  /** fetch data from the table: "admins" using primary key columns */
  admins_by_pk?: Maybe<Admins>;
  /** fetch data from the table: "authors" */
  authors: Array<Authors>;
  /** fetch aggregated fields from the table: "authors" */
  authors_aggregate: Authors_Aggregate;
  /** fetch data from the table: "authors" using primary key columns */
  authors_by_pk?: Maybe<Authors>;
  /** fetch data from the table: "comments" */
  comments: Array<Comments>;
  /** fetch aggregated fields from the table: "comments" */
  comments_aggregate: Comments_Aggregate;
  /** fetch data from the table: "comments" using primary key columns */
  comments_by_pk?: Maybe<Comments>;
  /** fetch data from the table: "news" */
  news: Array<News>;
  /** fetch aggregated fields from the table: "news" */
  news_aggregate: News_Aggregate;
  /** fetch data from the table: "news" using primary key columns */
  news_by_pk?: Maybe<News>;
  /** fetch data from the table: "reactions" */
  reactions: Array<Reactions>;
  /** fetch aggregated fields from the table: "reactions" */
  reactions_aggregate: Reactions_Aggregate;
  /** fetch data from the table: "reactions" using primary key columns */
  reactions_by_pk?: Maybe<Reactions>;
  /** fetch data from the table: "vw_detail_news" */
  vw_detail_news: Array<Vw_Detail_News>;
  /** fetch aggregated fields from the table: "vw_detail_news" */
  vw_detail_news_aggregate: Vw_Detail_News_Aggregate;
  /** fetch data from the table: "vw_news_by_week" */
  vw_news_by_week: Array<Vw_News_By_Week>;
  /** fetch aggregated fields from the table: "vw_news_by_week" */
  vw_news_by_week_aggregate: Vw_News_By_Week_Aggregate;
  /** fetch data from the table: "vw_type_name" */
  vw_type_name: Array<Vw_Type_Name>;
  /** fetch aggregated fields from the table: "vw_type_name" */
  vw_type_name_aggregate: Vw_Type_Name_Aggregate;
};


/** subscription root */
export type Subscription_RootAdminsArgs = {
  distinct_on?: Maybe<Array<Admins_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Admins_Order_By>>;
  where?: Maybe<Admins_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAdmins_AggregateArgs = {
  distinct_on?: Maybe<Array<Admins_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Admins_Order_By>>;
  where?: Maybe<Admins_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAdmins_By_PkArgs = {
  id: Scalars['Int'];
};


/** subscription root */
export type Subscription_RootAuthorsArgs = {
  distinct_on?: Maybe<Array<Authors_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Authors_Order_By>>;
  where?: Maybe<Authors_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuthors_AggregateArgs = {
  distinct_on?: Maybe<Array<Authors_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Authors_Order_By>>;
  where?: Maybe<Authors_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuthors_By_PkArgs = {
  id: Scalars['Int'];
};


/** subscription root */
export type Subscription_RootCommentsArgs = {
  distinct_on?: Maybe<Array<Comments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Comments_Order_By>>;
  where?: Maybe<Comments_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootComments_AggregateArgs = {
  distinct_on?: Maybe<Array<Comments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Comments_Order_By>>;
  where?: Maybe<Comments_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootComments_By_PkArgs = {
  id: Scalars['Int'];
};


/** subscription root */
export type Subscription_RootNewsArgs = {
  distinct_on?: Maybe<Array<News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<News_Order_By>>;
  where?: Maybe<News_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootNews_AggregateArgs = {
  distinct_on?: Maybe<Array<News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<News_Order_By>>;
  where?: Maybe<News_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootNews_By_PkArgs = {
  id: Scalars['Int'];
};


/** subscription root */
export type Subscription_RootReactionsArgs = {
  distinct_on?: Maybe<Array<Reactions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Reactions_Order_By>>;
  where?: Maybe<Reactions_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootReactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Reactions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Reactions_Order_By>>;
  where?: Maybe<Reactions_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootReactions_By_PkArgs = {
  id: Scalars['Int'];
};


/** subscription root */
export type Subscription_RootVw_Detail_NewsArgs = {
  distinct_on?: Maybe<Array<Vw_Detail_News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Detail_News_Order_By>>;
  where?: Maybe<Vw_Detail_News_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootVw_Detail_News_AggregateArgs = {
  distinct_on?: Maybe<Array<Vw_Detail_News_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Detail_News_Order_By>>;
  where?: Maybe<Vw_Detail_News_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootVw_News_By_WeekArgs = {
  distinct_on?: Maybe<Array<Vw_News_By_Week_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_News_By_Week_Order_By>>;
  where?: Maybe<Vw_News_By_Week_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootVw_News_By_Week_AggregateArgs = {
  distinct_on?: Maybe<Array<Vw_News_By_Week_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_News_By_Week_Order_By>>;
  where?: Maybe<Vw_News_By_Week_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootVw_Type_NameArgs = {
  distinct_on?: Maybe<Array<Vw_Type_Name_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Type_Name_Order_By>>;
  where?: Maybe<Vw_Type_Name_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootVw_Type_Name_AggregateArgs = {
  distinct_on?: Maybe<Array<Vw_Type_Name_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Vw_Type_Name_Order_By>>;
  where?: Maybe<Vw_Type_Name_Bool_Exp>;
};


/** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamptz']>;
  _gt?: Maybe<Scalars['timestamptz']>;
  _gte?: Maybe<Scalars['timestamptz']>;
  _in?: Maybe<Array<Scalars['timestamptz']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['timestamptz']>;
  _lte?: Maybe<Scalars['timestamptz']>;
  _neq?: Maybe<Scalars['timestamptz']>;
  _nin?: Maybe<Array<Scalars['timestamptz']>>;
};

/** columns and relationships of "vw_detail_news" */
export type Vw_Detail_News = {
   __typename?: 'vw_detail_news';
  created_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  img_url?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  views?: Maybe<Scalars['Int']>;
};

/** aggregated selection of "vw_detail_news" */
export type Vw_Detail_News_Aggregate = {
   __typename?: 'vw_detail_news_aggregate';
  aggregate?: Maybe<Vw_Detail_News_Aggregate_Fields>;
  nodes: Array<Vw_Detail_News>;
};

/** aggregate fields of "vw_detail_news" */
export type Vw_Detail_News_Aggregate_Fields = {
   __typename?: 'vw_detail_news_aggregate_fields';
  avg?: Maybe<Vw_Detail_News_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Vw_Detail_News_Max_Fields>;
  min?: Maybe<Vw_Detail_News_Min_Fields>;
  stddev?: Maybe<Vw_Detail_News_Stddev_Fields>;
  stddev_pop?: Maybe<Vw_Detail_News_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Vw_Detail_News_Stddev_Samp_Fields>;
  sum?: Maybe<Vw_Detail_News_Sum_Fields>;
  var_pop?: Maybe<Vw_Detail_News_Var_Pop_Fields>;
  var_samp?: Maybe<Vw_Detail_News_Var_Samp_Fields>;
  variance?: Maybe<Vw_Detail_News_Variance_Fields>;
};


/** aggregate fields of "vw_detail_news" */
export type Vw_Detail_News_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Vw_Detail_News_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "vw_detail_news" */
export type Vw_Detail_News_Aggregate_Order_By = {
  avg?: Maybe<Vw_Detail_News_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Vw_Detail_News_Max_Order_By>;
  min?: Maybe<Vw_Detail_News_Min_Order_By>;
  stddev?: Maybe<Vw_Detail_News_Stddev_Order_By>;
  stddev_pop?: Maybe<Vw_Detail_News_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Vw_Detail_News_Stddev_Samp_Order_By>;
  sum?: Maybe<Vw_Detail_News_Sum_Order_By>;
  var_pop?: Maybe<Vw_Detail_News_Var_Pop_Order_By>;
  var_samp?: Maybe<Vw_Detail_News_Var_Samp_Order_By>;
  variance?: Maybe<Vw_Detail_News_Variance_Order_By>;
};

/** aggregate avg on columns */
export type Vw_Detail_News_Avg_Fields = {
   __typename?: 'vw_detail_news_avg_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Avg_Order_By = {
  views?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "vw_detail_news". All fields are combined with a logical 'AND'. */
export type Vw_Detail_News_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Vw_Detail_News_Bool_Exp>>>;
  _not?: Maybe<Vw_Detail_News_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Vw_Detail_News_Bool_Exp>>>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  description?: Maybe<String_Comparison_Exp>;
  img_url?: Maybe<String_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  news_detail?: Maybe<String_Comparison_Exp>;
  title?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
  views?: Maybe<Int_Comparison_Exp>;
};

/** aggregate max on columns */
export type Vw_Detail_News_Max_Fields = {
   __typename?: 'vw_detail_news_max_fields';
  created_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  img_url?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  news_detail?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Vw_Detail_News_Min_Fields = {
   __typename?: 'vw_detail_news_min_fields';
  created_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  img_url?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  news_detail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  news_detail?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** ordering options when selecting data from "vw_detail_news" */
export type Vw_Detail_News_Order_By = {
  created_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  news_detail?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** select columns of table "vw_detail_news" */
export enum Vw_Detail_News_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  ImgUrl = 'img_url',
  /** column name */
  Name = 'name',
  /** column name */
  NewsDetail = 'news_detail',
  /** column name */
  Title = 'title',
  /** column name */
  Type = 'type',
  /** column name */
  Views = 'views'
}

/** aggregate stddev on columns */
export type Vw_Detail_News_Stddev_Fields = {
   __typename?: 'vw_detail_news_stddev_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Stddev_Order_By = {
  views?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Vw_Detail_News_Stddev_Pop_Fields = {
   __typename?: 'vw_detail_news_stddev_pop_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Stddev_Pop_Order_By = {
  views?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Vw_Detail_News_Stddev_Samp_Fields = {
   __typename?: 'vw_detail_news_stddev_samp_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Stddev_Samp_Order_By = {
  views?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Vw_Detail_News_Sum_Fields = {
   __typename?: 'vw_detail_news_sum_fields';
  views?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Sum_Order_By = {
  views?: Maybe<Order_By>;
};

/** aggregate var_pop on columns */
export type Vw_Detail_News_Var_Pop_Fields = {
   __typename?: 'vw_detail_news_var_pop_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Var_Pop_Order_By = {
  views?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Vw_Detail_News_Var_Samp_Fields = {
   __typename?: 'vw_detail_news_var_samp_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Var_Samp_Order_By = {
  views?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Vw_Detail_News_Variance_Fields = {
   __typename?: 'vw_detail_news_variance_fields';
  views?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "vw_detail_news" */
export type Vw_Detail_News_Variance_Order_By = {
  views?: Maybe<Order_By>;
};

/** columns and relationships of "vw_news_by_week" */
export type Vw_News_By_Week = {
   __typename?: 'vw_news_by_week';
  created_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  views?: Maybe<Scalars['Int']>;
};

/** aggregated selection of "vw_news_by_week" */
export type Vw_News_By_Week_Aggregate = {
   __typename?: 'vw_news_by_week_aggregate';
  aggregate?: Maybe<Vw_News_By_Week_Aggregate_Fields>;
  nodes: Array<Vw_News_By_Week>;
};

/** aggregate fields of "vw_news_by_week" */
export type Vw_News_By_Week_Aggregate_Fields = {
   __typename?: 'vw_news_by_week_aggregate_fields';
  avg?: Maybe<Vw_News_By_Week_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Vw_News_By_Week_Max_Fields>;
  min?: Maybe<Vw_News_By_Week_Min_Fields>;
  stddev?: Maybe<Vw_News_By_Week_Stddev_Fields>;
  stddev_pop?: Maybe<Vw_News_By_Week_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Vw_News_By_Week_Stddev_Samp_Fields>;
  sum?: Maybe<Vw_News_By_Week_Sum_Fields>;
  var_pop?: Maybe<Vw_News_By_Week_Var_Pop_Fields>;
  var_samp?: Maybe<Vw_News_By_Week_Var_Samp_Fields>;
  variance?: Maybe<Vw_News_By_Week_Variance_Fields>;
};


/** aggregate fields of "vw_news_by_week" */
export type Vw_News_By_Week_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Vw_News_By_Week_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "vw_news_by_week" */
export type Vw_News_By_Week_Aggregate_Order_By = {
  avg?: Maybe<Vw_News_By_Week_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Vw_News_By_Week_Max_Order_By>;
  min?: Maybe<Vw_News_By_Week_Min_Order_By>;
  stddev?: Maybe<Vw_News_By_Week_Stddev_Order_By>;
  stddev_pop?: Maybe<Vw_News_By_Week_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Vw_News_By_Week_Stddev_Samp_Order_By>;
  sum?: Maybe<Vw_News_By_Week_Sum_Order_By>;
  var_pop?: Maybe<Vw_News_By_Week_Var_Pop_Order_By>;
  var_samp?: Maybe<Vw_News_By_Week_Var_Samp_Order_By>;
  variance?: Maybe<Vw_News_By_Week_Variance_Order_By>;
};

/** aggregate avg on columns */
export type Vw_News_By_Week_Avg_Fields = {
   __typename?: 'vw_news_by_week_avg_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Avg_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "vw_news_by_week". All fields are combined with a logical 'AND'. */
export type Vw_News_By_Week_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Vw_News_By_Week_Bool_Exp>>>;
  _not?: Maybe<Vw_News_By_Week_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Vw_News_By_Week_Bool_Exp>>>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  description?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  img_url?: Maybe<String_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  title?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
  views?: Maybe<Int_Comparison_Exp>;
};

/** aggregate max on columns */
export type Vw_News_By_Week_Max_Fields = {
   __typename?: 'vw_news_by_week_max_fields';
  created_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Vw_News_By_Week_Min_Fields = {
   __typename?: 'vw_news_by_week_min_fields';
  created_at?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  img_url?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** ordering options when selecting data from "vw_news_by_week" */
export type Vw_News_By_Week_Order_By = {
  created_at?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  img_url?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** select columns of table "vw_news_by_week" */
export enum Vw_News_By_Week_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  ImgUrl = 'img_url',
  /** column name */
  Name = 'name',
  /** column name */
  Title = 'title',
  /** column name */
  Type = 'type',
  /** column name */
  Views = 'views'
}

/** aggregate stddev on columns */
export type Vw_News_By_Week_Stddev_Fields = {
   __typename?: 'vw_news_by_week_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Stddev_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Vw_News_By_Week_Stddev_Pop_Fields = {
   __typename?: 'vw_news_by_week_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Stddev_Pop_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Vw_News_By_Week_Stddev_Samp_Fields = {
   __typename?: 'vw_news_by_week_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Stddev_Samp_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Vw_News_By_Week_Sum_Fields = {
   __typename?: 'vw_news_by_week_sum_fields';
  id?: Maybe<Scalars['Int']>;
  views?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Sum_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate var_pop on columns */
export type Vw_News_By_Week_Var_Pop_Fields = {
   __typename?: 'vw_news_by_week_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Var_Pop_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Vw_News_By_Week_Var_Samp_Fields = {
   __typename?: 'vw_news_by_week_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Var_Samp_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Vw_News_By_Week_Variance_Fields = {
   __typename?: 'vw_news_by_week_variance_fields';
  id?: Maybe<Scalars['Float']>;
  views?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "vw_news_by_week" */
export type Vw_News_By_Week_Variance_Order_By = {
  id?: Maybe<Order_By>;
  views?: Maybe<Order_By>;
};

/** columns and relationships of "vw_type_name" */
export type Vw_Type_Name = {
   __typename?: 'vw_type_name';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** aggregated selection of "vw_type_name" */
export type Vw_Type_Name_Aggregate = {
   __typename?: 'vw_type_name_aggregate';
  aggregate?: Maybe<Vw_Type_Name_Aggregate_Fields>;
  nodes: Array<Vw_Type_Name>;
};

/** aggregate fields of "vw_type_name" */
export type Vw_Type_Name_Aggregate_Fields = {
   __typename?: 'vw_type_name_aggregate_fields';
  avg?: Maybe<Vw_Type_Name_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Vw_Type_Name_Max_Fields>;
  min?: Maybe<Vw_Type_Name_Min_Fields>;
  stddev?: Maybe<Vw_Type_Name_Stddev_Fields>;
  stddev_pop?: Maybe<Vw_Type_Name_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Vw_Type_Name_Stddev_Samp_Fields>;
  sum?: Maybe<Vw_Type_Name_Sum_Fields>;
  var_pop?: Maybe<Vw_Type_Name_Var_Pop_Fields>;
  var_samp?: Maybe<Vw_Type_Name_Var_Samp_Fields>;
  variance?: Maybe<Vw_Type_Name_Variance_Fields>;
};


/** aggregate fields of "vw_type_name" */
export type Vw_Type_Name_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Vw_Type_Name_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "vw_type_name" */
export type Vw_Type_Name_Aggregate_Order_By = {
  avg?: Maybe<Vw_Type_Name_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Vw_Type_Name_Max_Order_By>;
  min?: Maybe<Vw_Type_Name_Min_Order_By>;
  stddev?: Maybe<Vw_Type_Name_Stddev_Order_By>;
  stddev_pop?: Maybe<Vw_Type_Name_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Vw_Type_Name_Stddev_Samp_Order_By>;
  sum?: Maybe<Vw_Type_Name_Sum_Order_By>;
  var_pop?: Maybe<Vw_Type_Name_Var_Pop_Order_By>;
  var_samp?: Maybe<Vw_Type_Name_Var_Samp_Order_By>;
  variance?: Maybe<Vw_Type_Name_Variance_Order_By>;
};

/** aggregate avg on columns */
export type Vw_Type_Name_Avg_Fields = {
   __typename?: 'vw_type_name_avg_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "vw_type_name" */
export type Vw_Type_Name_Avg_Order_By = {
  id?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "vw_type_name". All fields are combined with a logical 'AND'. */
export type Vw_Type_Name_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Vw_Type_Name_Bool_Exp>>>;
  _not?: Maybe<Vw_Type_Name_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Vw_Type_Name_Bool_Exp>>>;
  id?: Maybe<Int_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
};

/** aggregate max on columns */
export type Vw_Type_Name_Max_Fields = {
   __typename?: 'vw_type_name_max_fields';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "vw_type_name" */
export type Vw_Type_Name_Max_Order_By = {
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Vw_Type_Name_Min_Fields = {
   __typename?: 'vw_type_name_min_fields';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "vw_type_name" */
export type Vw_Type_Name_Min_Order_By = {
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
};

/** ordering options when selecting data from "vw_type_name" */
export type Vw_Type_Name_Order_By = {
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
};

/** select columns of table "vw_type_name" */
export enum Vw_Type_Name_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Type = 'type'
}

/** aggregate stddev on columns */
export type Vw_Type_Name_Stddev_Fields = {
   __typename?: 'vw_type_name_stddev_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "vw_type_name" */
export type Vw_Type_Name_Stddev_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Vw_Type_Name_Stddev_Pop_Fields = {
   __typename?: 'vw_type_name_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "vw_type_name" */
export type Vw_Type_Name_Stddev_Pop_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Vw_Type_Name_Stddev_Samp_Fields = {
   __typename?: 'vw_type_name_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "vw_type_name" */
export type Vw_Type_Name_Stddev_Samp_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Vw_Type_Name_Sum_Fields = {
   __typename?: 'vw_type_name_sum_fields';
  id?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "vw_type_name" */
export type Vw_Type_Name_Sum_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate var_pop on columns */
export type Vw_Type_Name_Var_Pop_Fields = {
   __typename?: 'vw_type_name_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "vw_type_name" */
export type Vw_Type_Name_Var_Pop_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Vw_Type_Name_Var_Samp_Fields = {
   __typename?: 'vw_type_name_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "vw_type_name" */
export type Vw_Type_Name_Var_Samp_Order_By = {
  id?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Vw_Type_Name_Variance_Fields = {
   __typename?: 'vw_type_name_variance_fields';
  id?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "vw_type_name" */
export type Vw_Type_Name_Variance_Order_By = {
  id?: Maybe<Order_By>;
};

export type Get_NewsQueryVariables = {};


export type Get_NewsQuery = (
  { __typename?: 'query_root' }
  & { news: Array<(
    { __typename?: 'news' }
    & Pick<News, 'created_at' | 'description' | 'title' | 'type'>
  )> }
);

export type AuthorsQueryVariables = {};


export type AuthorsQuery = (
  { __typename?: 'query_root' }
  & { authors: Array<(
    { __typename?: 'authors' }
    & Pick<Authors, 'name' | 'id'>
  )> }
);



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type isTypeOfResolverFn<T = {}> = (obj: T, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  String: ResolverTypeWrapper<Scalars['String']>,
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>,
  Int_comparison_exp: Int_Comparison_Exp,
  Int: ResolverTypeWrapper<Scalars['Int']>,
  String_comparison_exp: String_Comparison_Exp,
  admins: ResolverTypeWrapper<Admins>,
  admins_aggregate: ResolverTypeWrapper<Admins_Aggregate>,
  admins_aggregate_fields: ResolverTypeWrapper<Admins_Aggregate_Fields>,
  admins_aggregate_order_by: Admins_Aggregate_Order_By,
  admins_arr_rel_insert_input: Admins_Arr_Rel_Insert_Input,
  admins_avg_fields: ResolverTypeWrapper<Admins_Avg_Fields>,
  Float: ResolverTypeWrapper<Scalars['Float']>,
  admins_avg_order_by: Admins_Avg_Order_By,
  admins_bool_exp: Admins_Bool_Exp,
  admins_constraint: Admins_Constraint,
  admins_inc_input: Admins_Inc_Input,
  admins_insert_input: Admins_Insert_Input,
  admins_max_fields: ResolverTypeWrapper<Admins_Max_Fields>,
  admins_max_order_by: Admins_Max_Order_By,
  admins_min_fields: ResolverTypeWrapper<Admins_Min_Fields>,
  admins_min_order_by: Admins_Min_Order_By,
  admins_mutation_response: ResolverTypeWrapper<Admins_Mutation_Response>,
  admins_obj_rel_insert_input: Admins_Obj_Rel_Insert_Input,
  admins_on_conflict: Admins_On_Conflict,
  admins_order_by: Admins_Order_By,
  admins_select_column: Admins_Select_Column,
  admins_set_input: Admins_Set_Input,
  admins_stddev_fields: ResolverTypeWrapper<Admins_Stddev_Fields>,
  admins_stddev_order_by: Admins_Stddev_Order_By,
  admins_stddev_pop_fields: ResolverTypeWrapper<Admins_Stddev_Pop_Fields>,
  admins_stddev_pop_order_by: Admins_Stddev_Pop_Order_By,
  admins_stddev_samp_fields: ResolverTypeWrapper<Admins_Stddev_Samp_Fields>,
  admins_stddev_samp_order_by: Admins_Stddev_Samp_Order_By,
  admins_sum_fields: ResolverTypeWrapper<Admins_Sum_Fields>,
  admins_sum_order_by: Admins_Sum_Order_By,
  admins_update_column: Admins_Update_Column,
  admins_var_pop_fields: ResolverTypeWrapper<Admins_Var_Pop_Fields>,
  admins_var_pop_order_by: Admins_Var_Pop_Order_By,
  admins_var_samp_fields: ResolverTypeWrapper<Admins_Var_Samp_Fields>,
  admins_var_samp_order_by: Admins_Var_Samp_Order_By,
  admins_variance_fields: ResolverTypeWrapper<Admins_Variance_Fields>,
  admins_variance_order_by: Admins_Variance_Order_By,
  authors: ResolverTypeWrapper<Authors>,
  authors_aggregate: ResolverTypeWrapper<Authors_Aggregate>,
  authors_aggregate_fields: ResolverTypeWrapper<Authors_Aggregate_Fields>,
  authors_aggregate_order_by: Authors_Aggregate_Order_By,
  authors_arr_rel_insert_input: Authors_Arr_Rel_Insert_Input,
  authors_avg_fields: ResolverTypeWrapper<Authors_Avg_Fields>,
  authors_avg_order_by: Authors_Avg_Order_By,
  authors_bool_exp: Authors_Bool_Exp,
  authors_constraint: Authors_Constraint,
  authors_inc_input: Authors_Inc_Input,
  authors_insert_input: Authors_Insert_Input,
  authors_max_fields: ResolverTypeWrapper<Authors_Max_Fields>,
  authors_max_order_by: Authors_Max_Order_By,
  authors_min_fields: ResolverTypeWrapper<Authors_Min_Fields>,
  authors_min_order_by: Authors_Min_Order_By,
  authors_mutation_response: ResolverTypeWrapper<Authors_Mutation_Response>,
  authors_obj_rel_insert_input: Authors_Obj_Rel_Insert_Input,
  authors_on_conflict: Authors_On_Conflict,
  authors_order_by: Authors_Order_By,
  authors_select_column: Authors_Select_Column,
  authors_set_input: Authors_Set_Input,
  authors_stddev_fields: ResolverTypeWrapper<Authors_Stddev_Fields>,
  authors_stddev_order_by: Authors_Stddev_Order_By,
  authors_stddev_pop_fields: ResolverTypeWrapper<Authors_Stddev_Pop_Fields>,
  authors_stddev_pop_order_by: Authors_Stddev_Pop_Order_By,
  authors_stddev_samp_fields: ResolverTypeWrapper<Authors_Stddev_Samp_Fields>,
  authors_stddev_samp_order_by: Authors_Stddev_Samp_Order_By,
  authors_sum_fields: ResolverTypeWrapper<Authors_Sum_Fields>,
  authors_sum_order_by: Authors_Sum_Order_By,
  authors_update_column: Authors_Update_Column,
  authors_var_pop_fields: ResolverTypeWrapper<Authors_Var_Pop_Fields>,
  authors_var_pop_order_by: Authors_Var_Pop_Order_By,
  authors_var_samp_fields: ResolverTypeWrapper<Authors_Var_Samp_Fields>,
  authors_var_samp_order_by: Authors_Var_Samp_Order_By,
  authors_variance_fields: ResolverTypeWrapper<Authors_Variance_Fields>,
  authors_variance_order_by: Authors_Variance_Order_By,
  comments: ResolverTypeWrapper<Comments>,
  comments_aggregate: ResolverTypeWrapper<Comments_Aggregate>,
  comments_aggregate_fields: ResolverTypeWrapper<Comments_Aggregate_Fields>,
  comments_aggregate_order_by: Comments_Aggregate_Order_By,
  comments_arr_rel_insert_input: Comments_Arr_Rel_Insert_Input,
  comments_avg_fields: ResolverTypeWrapper<Comments_Avg_Fields>,
  comments_avg_order_by: Comments_Avg_Order_By,
  comments_bool_exp: Comments_Bool_Exp,
  comments_constraint: Comments_Constraint,
  comments_inc_input: Comments_Inc_Input,
  comments_insert_input: Comments_Insert_Input,
  comments_max_fields: ResolverTypeWrapper<Comments_Max_Fields>,
  comments_max_order_by: Comments_Max_Order_By,
  comments_min_fields: ResolverTypeWrapper<Comments_Min_Fields>,
  comments_min_order_by: Comments_Min_Order_By,
  comments_mutation_response: ResolverTypeWrapper<Comments_Mutation_Response>,
  comments_obj_rel_insert_input: Comments_Obj_Rel_Insert_Input,
  comments_on_conflict: Comments_On_Conflict,
  comments_order_by: Comments_Order_By,
  comments_select_column: Comments_Select_Column,
  comments_set_input: Comments_Set_Input,
  comments_stddev_fields: ResolverTypeWrapper<Comments_Stddev_Fields>,
  comments_stddev_order_by: Comments_Stddev_Order_By,
  comments_stddev_pop_fields: ResolverTypeWrapper<Comments_Stddev_Pop_Fields>,
  comments_stddev_pop_order_by: Comments_Stddev_Pop_Order_By,
  comments_stddev_samp_fields: ResolverTypeWrapper<Comments_Stddev_Samp_Fields>,
  comments_stddev_samp_order_by: Comments_Stddev_Samp_Order_By,
  comments_sum_fields: ResolverTypeWrapper<Comments_Sum_Fields>,
  comments_sum_order_by: Comments_Sum_Order_By,
  comments_update_column: Comments_Update_Column,
  comments_var_pop_fields: ResolverTypeWrapper<Comments_Var_Pop_Fields>,
  comments_var_pop_order_by: Comments_Var_Pop_Order_By,
  comments_var_samp_fields: ResolverTypeWrapper<Comments_Var_Samp_Fields>,
  comments_var_samp_order_by: Comments_Var_Samp_Order_By,
  comments_variance_fields: ResolverTypeWrapper<Comments_Variance_Fields>,
  comments_variance_order_by: Comments_Variance_Order_By,
  date: ResolverTypeWrapper<Scalars['date']>,
  date_comparison_exp: Date_Comparison_Exp,
  mutation_root: ResolverTypeWrapper<{}>,
  news: ResolverTypeWrapper<News>,
  news_aggregate: ResolverTypeWrapper<News_Aggregate>,
  news_aggregate_fields: ResolverTypeWrapper<News_Aggregate_Fields>,
  news_aggregate_order_by: News_Aggregate_Order_By,
  news_arr_rel_insert_input: News_Arr_Rel_Insert_Input,
  news_avg_fields: ResolverTypeWrapper<News_Avg_Fields>,
  news_avg_order_by: News_Avg_Order_By,
  news_bool_exp: News_Bool_Exp,
  news_constraint: News_Constraint,
  news_inc_input: News_Inc_Input,
  news_insert_input: News_Insert_Input,
  news_max_fields: ResolverTypeWrapper<News_Max_Fields>,
  news_max_order_by: News_Max_Order_By,
  news_min_fields: ResolverTypeWrapper<News_Min_Fields>,
  news_min_order_by: News_Min_Order_By,
  news_mutation_response: ResolverTypeWrapper<News_Mutation_Response>,
  news_obj_rel_insert_input: News_Obj_Rel_Insert_Input,
  news_on_conflict: News_On_Conflict,
  news_order_by: News_Order_By,
  news_select_column: News_Select_Column,
  news_set_input: News_Set_Input,
  news_stddev_fields: ResolverTypeWrapper<News_Stddev_Fields>,
  news_stddev_order_by: News_Stddev_Order_By,
  news_stddev_pop_fields: ResolverTypeWrapper<News_Stddev_Pop_Fields>,
  news_stddev_pop_order_by: News_Stddev_Pop_Order_By,
  news_stddev_samp_fields: ResolverTypeWrapper<News_Stddev_Samp_Fields>,
  news_stddev_samp_order_by: News_Stddev_Samp_Order_By,
  news_sum_fields: ResolverTypeWrapper<News_Sum_Fields>,
  news_sum_order_by: News_Sum_Order_By,
  news_update_column: News_Update_Column,
  news_var_pop_fields: ResolverTypeWrapper<News_Var_Pop_Fields>,
  news_var_pop_order_by: News_Var_Pop_Order_By,
  news_var_samp_fields: ResolverTypeWrapper<News_Var_Samp_Fields>,
  news_var_samp_order_by: News_Var_Samp_Order_By,
  news_variance_fields: ResolverTypeWrapper<News_Variance_Fields>,
  news_variance_order_by: News_Variance_Order_By,
  order_by: Order_By,
  query_root: ResolverTypeWrapper<{}>,
  reactions: ResolverTypeWrapper<Reactions>,
  reactions_aggregate: ResolverTypeWrapper<Reactions_Aggregate>,
  reactions_aggregate_fields: ResolverTypeWrapper<Reactions_Aggregate_Fields>,
  reactions_aggregate_order_by: Reactions_Aggregate_Order_By,
  reactions_arr_rel_insert_input: Reactions_Arr_Rel_Insert_Input,
  reactions_avg_fields: ResolverTypeWrapper<Reactions_Avg_Fields>,
  reactions_avg_order_by: Reactions_Avg_Order_By,
  reactions_bool_exp: Reactions_Bool_Exp,
  reactions_constraint: Reactions_Constraint,
  reactions_inc_input: Reactions_Inc_Input,
  reactions_insert_input: Reactions_Insert_Input,
  reactions_max_fields: ResolverTypeWrapper<Reactions_Max_Fields>,
  reactions_max_order_by: Reactions_Max_Order_By,
  reactions_min_fields: ResolverTypeWrapper<Reactions_Min_Fields>,
  reactions_min_order_by: Reactions_Min_Order_By,
  reactions_mutation_response: ResolverTypeWrapper<Reactions_Mutation_Response>,
  reactions_obj_rel_insert_input: Reactions_Obj_Rel_Insert_Input,
  reactions_on_conflict: Reactions_On_Conflict,
  reactions_order_by: Reactions_Order_By,
  reactions_select_column: Reactions_Select_Column,
  reactions_set_input: Reactions_Set_Input,
  reactions_stddev_fields: ResolverTypeWrapper<Reactions_Stddev_Fields>,
  reactions_stddev_order_by: Reactions_Stddev_Order_By,
  reactions_stddev_pop_fields: ResolverTypeWrapper<Reactions_Stddev_Pop_Fields>,
  reactions_stddev_pop_order_by: Reactions_Stddev_Pop_Order_By,
  reactions_stddev_samp_fields: ResolverTypeWrapper<Reactions_Stddev_Samp_Fields>,
  reactions_stddev_samp_order_by: Reactions_Stddev_Samp_Order_By,
  reactions_sum_fields: ResolverTypeWrapper<Reactions_Sum_Fields>,
  reactions_sum_order_by: Reactions_Sum_Order_By,
  reactions_update_column: Reactions_Update_Column,
  reactions_var_pop_fields: ResolverTypeWrapper<Reactions_Var_Pop_Fields>,
  reactions_var_pop_order_by: Reactions_Var_Pop_Order_By,
  reactions_var_samp_fields: ResolverTypeWrapper<Reactions_Var_Samp_Fields>,
  reactions_var_samp_order_by: Reactions_Var_Samp_Order_By,
  reactions_variance_fields: ResolverTypeWrapper<Reactions_Variance_Fields>,
  reactions_variance_order_by: Reactions_Variance_Order_By,
  subscription_root: ResolverTypeWrapper<{}>,
  timestamptz: ResolverTypeWrapper<Scalars['timestamptz']>,
  timestamptz_comparison_exp: Timestamptz_Comparison_Exp,
  vw_detail_news: ResolverTypeWrapper<Vw_Detail_News>,
  vw_detail_news_aggregate: ResolverTypeWrapper<Vw_Detail_News_Aggregate>,
  vw_detail_news_aggregate_fields: ResolverTypeWrapper<Vw_Detail_News_Aggregate_Fields>,
  vw_detail_news_aggregate_order_by: Vw_Detail_News_Aggregate_Order_By,
  vw_detail_news_avg_fields: ResolverTypeWrapper<Vw_Detail_News_Avg_Fields>,
  vw_detail_news_avg_order_by: Vw_Detail_News_Avg_Order_By,
  vw_detail_news_bool_exp: Vw_Detail_News_Bool_Exp,
  vw_detail_news_max_fields: ResolverTypeWrapper<Vw_Detail_News_Max_Fields>,
  vw_detail_news_max_order_by: Vw_Detail_News_Max_Order_By,
  vw_detail_news_min_fields: ResolverTypeWrapper<Vw_Detail_News_Min_Fields>,
  vw_detail_news_min_order_by: Vw_Detail_News_Min_Order_By,
  vw_detail_news_order_by: Vw_Detail_News_Order_By,
  vw_detail_news_select_column: Vw_Detail_News_Select_Column,
  vw_detail_news_stddev_fields: ResolverTypeWrapper<Vw_Detail_News_Stddev_Fields>,
  vw_detail_news_stddev_order_by: Vw_Detail_News_Stddev_Order_By,
  vw_detail_news_stddev_pop_fields: ResolverTypeWrapper<Vw_Detail_News_Stddev_Pop_Fields>,
  vw_detail_news_stddev_pop_order_by: Vw_Detail_News_Stddev_Pop_Order_By,
  vw_detail_news_stddev_samp_fields: ResolverTypeWrapper<Vw_Detail_News_Stddev_Samp_Fields>,
  vw_detail_news_stddev_samp_order_by: Vw_Detail_News_Stddev_Samp_Order_By,
  vw_detail_news_sum_fields: ResolverTypeWrapper<Vw_Detail_News_Sum_Fields>,
  vw_detail_news_sum_order_by: Vw_Detail_News_Sum_Order_By,
  vw_detail_news_var_pop_fields: ResolverTypeWrapper<Vw_Detail_News_Var_Pop_Fields>,
  vw_detail_news_var_pop_order_by: Vw_Detail_News_Var_Pop_Order_By,
  vw_detail_news_var_samp_fields: ResolverTypeWrapper<Vw_Detail_News_Var_Samp_Fields>,
  vw_detail_news_var_samp_order_by: Vw_Detail_News_Var_Samp_Order_By,
  vw_detail_news_variance_fields: ResolverTypeWrapper<Vw_Detail_News_Variance_Fields>,
  vw_detail_news_variance_order_by: Vw_Detail_News_Variance_Order_By,
  vw_news_by_week: ResolverTypeWrapper<Vw_News_By_Week>,
  vw_news_by_week_aggregate: ResolverTypeWrapper<Vw_News_By_Week_Aggregate>,
  vw_news_by_week_aggregate_fields: ResolverTypeWrapper<Vw_News_By_Week_Aggregate_Fields>,
  vw_news_by_week_aggregate_order_by: Vw_News_By_Week_Aggregate_Order_By,
  vw_news_by_week_avg_fields: ResolverTypeWrapper<Vw_News_By_Week_Avg_Fields>,
  vw_news_by_week_avg_order_by: Vw_News_By_Week_Avg_Order_By,
  vw_news_by_week_bool_exp: Vw_News_By_Week_Bool_Exp,
  vw_news_by_week_max_fields: ResolverTypeWrapper<Vw_News_By_Week_Max_Fields>,
  vw_news_by_week_max_order_by: Vw_News_By_Week_Max_Order_By,
  vw_news_by_week_min_fields: ResolverTypeWrapper<Vw_News_By_Week_Min_Fields>,
  vw_news_by_week_min_order_by: Vw_News_By_Week_Min_Order_By,
  vw_news_by_week_order_by: Vw_News_By_Week_Order_By,
  vw_news_by_week_select_column: Vw_News_By_Week_Select_Column,
  vw_news_by_week_stddev_fields: ResolverTypeWrapper<Vw_News_By_Week_Stddev_Fields>,
  vw_news_by_week_stddev_order_by: Vw_News_By_Week_Stddev_Order_By,
  vw_news_by_week_stddev_pop_fields: ResolverTypeWrapper<Vw_News_By_Week_Stddev_Pop_Fields>,
  vw_news_by_week_stddev_pop_order_by: Vw_News_By_Week_Stddev_Pop_Order_By,
  vw_news_by_week_stddev_samp_fields: ResolverTypeWrapper<Vw_News_By_Week_Stddev_Samp_Fields>,
  vw_news_by_week_stddev_samp_order_by: Vw_News_By_Week_Stddev_Samp_Order_By,
  vw_news_by_week_sum_fields: ResolverTypeWrapper<Vw_News_By_Week_Sum_Fields>,
  vw_news_by_week_sum_order_by: Vw_News_By_Week_Sum_Order_By,
  vw_news_by_week_var_pop_fields: ResolverTypeWrapper<Vw_News_By_Week_Var_Pop_Fields>,
  vw_news_by_week_var_pop_order_by: Vw_News_By_Week_Var_Pop_Order_By,
  vw_news_by_week_var_samp_fields: ResolverTypeWrapper<Vw_News_By_Week_Var_Samp_Fields>,
  vw_news_by_week_var_samp_order_by: Vw_News_By_Week_Var_Samp_Order_By,
  vw_news_by_week_variance_fields: ResolverTypeWrapper<Vw_News_By_Week_Variance_Fields>,
  vw_news_by_week_variance_order_by: Vw_News_By_Week_Variance_Order_By,
  vw_type_name: ResolverTypeWrapper<Vw_Type_Name>,
  vw_type_name_aggregate: ResolverTypeWrapper<Vw_Type_Name_Aggregate>,
  vw_type_name_aggregate_fields: ResolverTypeWrapper<Vw_Type_Name_Aggregate_Fields>,
  vw_type_name_aggregate_order_by: Vw_Type_Name_Aggregate_Order_By,
  vw_type_name_avg_fields: ResolverTypeWrapper<Vw_Type_Name_Avg_Fields>,
  vw_type_name_avg_order_by: Vw_Type_Name_Avg_Order_By,
  vw_type_name_bool_exp: Vw_Type_Name_Bool_Exp,
  vw_type_name_max_fields: ResolverTypeWrapper<Vw_Type_Name_Max_Fields>,
  vw_type_name_max_order_by: Vw_Type_Name_Max_Order_By,
  vw_type_name_min_fields: ResolverTypeWrapper<Vw_Type_Name_Min_Fields>,
  vw_type_name_min_order_by: Vw_Type_Name_Min_Order_By,
  vw_type_name_order_by: Vw_Type_Name_Order_By,
  vw_type_name_select_column: Vw_Type_Name_Select_Column,
  vw_type_name_stddev_fields: ResolverTypeWrapper<Vw_Type_Name_Stddev_Fields>,
  vw_type_name_stddev_order_by: Vw_Type_Name_Stddev_Order_By,
  vw_type_name_stddev_pop_fields: ResolverTypeWrapper<Vw_Type_Name_Stddev_Pop_Fields>,
  vw_type_name_stddev_pop_order_by: Vw_Type_Name_Stddev_Pop_Order_By,
  vw_type_name_stddev_samp_fields: ResolverTypeWrapper<Vw_Type_Name_Stddev_Samp_Fields>,
  vw_type_name_stddev_samp_order_by: Vw_Type_Name_Stddev_Samp_Order_By,
  vw_type_name_sum_fields: ResolverTypeWrapper<Vw_Type_Name_Sum_Fields>,
  vw_type_name_sum_order_by: Vw_Type_Name_Sum_Order_By,
  vw_type_name_var_pop_fields: ResolverTypeWrapper<Vw_Type_Name_Var_Pop_Fields>,
  vw_type_name_var_pop_order_by: Vw_Type_Name_Var_Pop_Order_By,
  vw_type_name_var_samp_fields: ResolverTypeWrapper<Vw_Type_Name_Var_Samp_Fields>,
  vw_type_name_var_samp_order_by: Vw_Type_Name_Var_Samp_Order_By,
  vw_type_name_variance_fields: ResolverTypeWrapper<Vw_Type_Name_Variance_Fields>,
  vw_type_name_variance_order_by: Vw_Type_Name_Variance_Order_By,
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  String: Scalars['String'],
  Boolean: Scalars['Boolean'],
  Int_comparison_exp: Int_Comparison_Exp,
  Int: Scalars['Int'],
  String_comparison_exp: String_Comparison_Exp,
  admins: Admins,
  admins_aggregate: Admins_Aggregate,
  admins_aggregate_fields: Admins_Aggregate_Fields,
  admins_aggregate_order_by: Admins_Aggregate_Order_By,
  admins_arr_rel_insert_input: Admins_Arr_Rel_Insert_Input,
  admins_avg_fields: Admins_Avg_Fields,
  Float: Scalars['Float'],
  admins_avg_order_by: Admins_Avg_Order_By,
  admins_bool_exp: Admins_Bool_Exp,
  admins_constraint: Admins_Constraint,
  admins_inc_input: Admins_Inc_Input,
  admins_insert_input: Admins_Insert_Input,
  admins_max_fields: Admins_Max_Fields,
  admins_max_order_by: Admins_Max_Order_By,
  admins_min_fields: Admins_Min_Fields,
  admins_min_order_by: Admins_Min_Order_By,
  admins_mutation_response: Admins_Mutation_Response,
  admins_obj_rel_insert_input: Admins_Obj_Rel_Insert_Input,
  admins_on_conflict: Admins_On_Conflict,
  admins_order_by: Admins_Order_By,
  admins_select_column: Admins_Select_Column,
  admins_set_input: Admins_Set_Input,
  admins_stddev_fields: Admins_Stddev_Fields,
  admins_stddev_order_by: Admins_Stddev_Order_By,
  admins_stddev_pop_fields: Admins_Stddev_Pop_Fields,
  admins_stddev_pop_order_by: Admins_Stddev_Pop_Order_By,
  admins_stddev_samp_fields: Admins_Stddev_Samp_Fields,
  admins_stddev_samp_order_by: Admins_Stddev_Samp_Order_By,
  admins_sum_fields: Admins_Sum_Fields,
  admins_sum_order_by: Admins_Sum_Order_By,
  admins_update_column: Admins_Update_Column,
  admins_var_pop_fields: Admins_Var_Pop_Fields,
  admins_var_pop_order_by: Admins_Var_Pop_Order_By,
  admins_var_samp_fields: Admins_Var_Samp_Fields,
  admins_var_samp_order_by: Admins_Var_Samp_Order_By,
  admins_variance_fields: Admins_Variance_Fields,
  admins_variance_order_by: Admins_Variance_Order_By,
  authors: Authors,
  authors_aggregate: Authors_Aggregate,
  authors_aggregate_fields: Authors_Aggregate_Fields,
  authors_aggregate_order_by: Authors_Aggregate_Order_By,
  authors_arr_rel_insert_input: Authors_Arr_Rel_Insert_Input,
  authors_avg_fields: Authors_Avg_Fields,
  authors_avg_order_by: Authors_Avg_Order_By,
  authors_bool_exp: Authors_Bool_Exp,
  authors_constraint: Authors_Constraint,
  authors_inc_input: Authors_Inc_Input,
  authors_insert_input: Authors_Insert_Input,
  authors_max_fields: Authors_Max_Fields,
  authors_max_order_by: Authors_Max_Order_By,
  authors_min_fields: Authors_Min_Fields,
  authors_min_order_by: Authors_Min_Order_By,
  authors_mutation_response: Authors_Mutation_Response,
  authors_obj_rel_insert_input: Authors_Obj_Rel_Insert_Input,
  authors_on_conflict: Authors_On_Conflict,
  authors_order_by: Authors_Order_By,
  authors_select_column: Authors_Select_Column,
  authors_set_input: Authors_Set_Input,
  authors_stddev_fields: Authors_Stddev_Fields,
  authors_stddev_order_by: Authors_Stddev_Order_By,
  authors_stddev_pop_fields: Authors_Stddev_Pop_Fields,
  authors_stddev_pop_order_by: Authors_Stddev_Pop_Order_By,
  authors_stddev_samp_fields: Authors_Stddev_Samp_Fields,
  authors_stddev_samp_order_by: Authors_Stddev_Samp_Order_By,
  authors_sum_fields: Authors_Sum_Fields,
  authors_sum_order_by: Authors_Sum_Order_By,
  authors_update_column: Authors_Update_Column,
  authors_var_pop_fields: Authors_Var_Pop_Fields,
  authors_var_pop_order_by: Authors_Var_Pop_Order_By,
  authors_var_samp_fields: Authors_Var_Samp_Fields,
  authors_var_samp_order_by: Authors_Var_Samp_Order_By,
  authors_variance_fields: Authors_Variance_Fields,
  authors_variance_order_by: Authors_Variance_Order_By,
  comments: Comments,
  comments_aggregate: Comments_Aggregate,
  comments_aggregate_fields: Comments_Aggregate_Fields,
  comments_aggregate_order_by: Comments_Aggregate_Order_By,
  comments_arr_rel_insert_input: Comments_Arr_Rel_Insert_Input,
  comments_avg_fields: Comments_Avg_Fields,
  comments_avg_order_by: Comments_Avg_Order_By,
  comments_bool_exp: Comments_Bool_Exp,
  comments_constraint: Comments_Constraint,
  comments_inc_input: Comments_Inc_Input,
  comments_insert_input: Comments_Insert_Input,
  comments_max_fields: Comments_Max_Fields,
  comments_max_order_by: Comments_Max_Order_By,
  comments_min_fields: Comments_Min_Fields,
  comments_min_order_by: Comments_Min_Order_By,
  comments_mutation_response: Comments_Mutation_Response,
  comments_obj_rel_insert_input: Comments_Obj_Rel_Insert_Input,
  comments_on_conflict: Comments_On_Conflict,
  comments_order_by: Comments_Order_By,
  comments_select_column: Comments_Select_Column,
  comments_set_input: Comments_Set_Input,
  comments_stddev_fields: Comments_Stddev_Fields,
  comments_stddev_order_by: Comments_Stddev_Order_By,
  comments_stddev_pop_fields: Comments_Stddev_Pop_Fields,
  comments_stddev_pop_order_by: Comments_Stddev_Pop_Order_By,
  comments_stddev_samp_fields: Comments_Stddev_Samp_Fields,
  comments_stddev_samp_order_by: Comments_Stddev_Samp_Order_By,
  comments_sum_fields: Comments_Sum_Fields,
  comments_sum_order_by: Comments_Sum_Order_By,
  comments_update_column: Comments_Update_Column,
  comments_var_pop_fields: Comments_Var_Pop_Fields,
  comments_var_pop_order_by: Comments_Var_Pop_Order_By,
  comments_var_samp_fields: Comments_Var_Samp_Fields,
  comments_var_samp_order_by: Comments_Var_Samp_Order_By,
  comments_variance_fields: Comments_Variance_Fields,
  comments_variance_order_by: Comments_Variance_Order_By,
  date: Scalars['date'],
  date_comparison_exp: Date_Comparison_Exp,
  mutation_root: {},
  news: News,
  news_aggregate: News_Aggregate,
  news_aggregate_fields: News_Aggregate_Fields,
  news_aggregate_order_by: News_Aggregate_Order_By,
  news_arr_rel_insert_input: News_Arr_Rel_Insert_Input,
  news_avg_fields: News_Avg_Fields,
  news_avg_order_by: News_Avg_Order_By,
  news_bool_exp: News_Bool_Exp,
  news_constraint: News_Constraint,
  news_inc_input: News_Inc_Input,
  news_insert_input: News_Insert_Input,
  news_max_fields: News_Max_Fields,
  news_max_order_by: News_Max_Order_By,
  news_min_fields: News_Min_Fields,
  news_min_order_by: News_Min_Order_By,
  news_mutation_response: News_Mutation_Response,
  news_obj_rel_insert_input: News_Obj_Rel_Insert_Input,
  news_on_conflict: News_On_Conflict,
  news_order_by: News_Order_By,
  news_select_column: News_Select_Column,
  news_set_input: News_Set_Input,
  news_stddev_fields: News_Stddev_Fields,
  news_stddev_order_by: News_Stddev_Order_By,
  news_stddev_pop_fields: News_Stddev_Pop_Fields,
  news_stddev_pop_order_by: News_Stddev_Pop_Order_By,
  news_stddev_samp_fields: News_Stddev_Samp_Fields,
  news_stddev_samp_order_by: News_Stddev_Samp_Order_By,
  news_sum_fields: News_Sum_Fields,
  news_sum_order_by: News_Sum_Order_By,
  news_update_column: News_Update_Column,
  news_var_pop_fields: News_Var_Pop_Fields,
  news_var_pop_order_by: News_Var_Pop_Order_By,
  news_var_samp_fields: News_Var_Samp_Fields,
  news_var_samp_order_by: News_Var_Samp_Order_By,
  news_variance_fields: News_Variance_Fields,
  news_variance_order_by: News_Variance_Order_By,
  order_by: Order_By,
  query_root: {},
  reactions: Reactions,
  reactions_aggregate: Reactions_Aggregate,
  reactions_aggregate_fields: Reactions_Aggregate_Fields,
  reactions_aggregate_order_by: Reactions_Aggregate_Order_By,
  reactions_arr_rel_insert_input: Reactions_Arr_Rel_Insert_Input,
  reactions_avg_fields: Reactions_Avg_Fields,
  reactions_avg_order_by: Reactions_Avg_Order_By,
  reactions_bool_exp: Reactions_Bool_Exp,
  reactions_constraint: Reactions_Constraint,
  reactions_inc_input: Reactions_Inc_Input,
  reactions_insert_input: Reactions_Insert_Input,
  reactions_max_fields: Reactions_Max_Fields,
  reactions_max_order_by: Reactions_Max_Order_By,
  reactions_min_fields: Reactions_Min_Fields,
  reactions_min_order_by: Reactions_Min_Order_By,
  reactions_mutation_response: Reactions_Mutation_Response,
  reactions_obj_rel_insert_input: Reactions_Obj_Rel_Insert_Input,
  reactions_on_conflict: Reactions_On_Conflict,
  reactions_order_by: Reactions_Order_By,
  reactions_select_column: Reactions_Select_Column,
  reactions_set_input: Reactions_Set_Input,
  reactions_stddev_fields: Reactions_Stddev_Fields,
  reactions_stddev_order_by: Reactions_Stddev_Order_By,
  reactions_stddev_pop_fields: Reactions_Stddev_Pop_Fields,
  reactions_stddev_pop_order_by: Reactions_Stddev_Pop_Order_By,
  reactions_stddev_samp_fields: Reactions_Stddev_Samp_Fields,
  reactions_stddev_samp_order_by: Reactions_Stddev_Samp_Order_By,
  reactions_sum_fields: Reactions_Sum_Fields,
  reactions_sum_order_by: Reactions_Sum_Order_By,
  reactions_update_column: Reactions_Update_Column,
  reactions_var_pop_fields: Reactions_Var_Pop_Fields,
  reactions_var_pop_order_by: Reactions_Var_Pop_Order_By,
  reactions_var_samp_fields: Reactions_Var_Samp_Fields,
  reactions_var_samp_order_by: Reactions_Var_Samp_Order_By,
  reactions_variance_fields: Reactions_Variance_Fields,
  reactions_variance_order_by: Reactions_Variance_Order_By,
  subscription_root: {},
  timestamptz: Scalars['timestamptz'],
  timestamptz_comparison_exp: Timestamptz_Comparison_Exp,
  vw_detail_news: Vw_Detail_News,
  vw_detail_news_aggregate: Vw_Detail_News_Aggregate,
  vw_detail_news_aggregate_fields: Vw_Detail_News_Aggregate_Fields,
  vw_detail_news_aggregate_order_by: Vw_Detail_News_Aggregate_Order_By,
  vw_detail_news_avg_fields: Vw_Detail_News_Avg_Fields,
  vw_detail_news_avg_order_by: Vw_Detail_News_Avg_Order_By,
  vw_detail_news_bool_exp: Vw_Detail_News_Bool_Exp,
  vw_detail_news_max_fields: Vw_Detail_News_Max_Fields,
  vw_detail_news_max_order_by: Vw_Detail_News_Max_Order_By,
  vw_detail_news_min_fields: Vw_Detail_News_Min_Fields,
  vw_detail_news_min_order_by: Vw_Detail_News_Min_Order_By,
  vw_detail_news_order_by: Vw_Detail_News_Order_By,
  vw_detail_news_select_column: Vw_Detail_News_Select_Column,
  vw_detail_news_stddev_fields: Vw_Detail_News_Stddev_Fields,
  vw_detail_news_stddev_order_by: Vw_Detail_News_Stddev_Order_By,
  vw_detail_news_stddev_pop_fields: Vw_Detail_News_Stddev_Pop_Fields,
  vw_detail_news_stddev_pop_order_by: Vw_Detail_News_Stddev_Pop_Order_By,
  vw_detail_news_stddev_samp_fields: Vw_Detail_News_Stddev_Samp_Fields,
  vw_detail_news_stddev_samp_order_by: Vw_Detail_News_Stddev_Samp_Order_By,
  vw_detail_news_sum_fields: Vw_Detail_News_Sum_Fields,
  vw_detail_news_sum_order_by: Vw_Detail_News_Sum_Order_By,
  vw_detail_news_var_pop_fields: Vw_Detail_News_Var_Pop_Fields,
  vw_detail_news_var_pop_order_by: Vw_Detail_News_Var_Pop_Order_By,
  vw_detail_news_var_samp_fields: Vw_Detail_News_Var_Samp_Fields,
  vw_detail_news_var_samp_order_by: Vw_Detail_News_Var_Samp_Order_By,
  vw_detail_news_variance_fields: Vw_Detail_News_Variance_Fields,
  vw_detail_news_variance_order_by: Vw_Detail_News_Variance_Order_By,
  vw_news_by_week: Vw_News_By_Week,
  vw_news_by_week_aggregate: Vw_News_By_Week_Aggregate,
  vw_news_by_week_aggregate_fields: Vw_News_By_Week_Aggregate_Fields,
  vw_news_by_week_aggregate_order_by: Vw_News_By_Week_Aggregate_Order_By,
  vw_news_by_week_avg_fields: Vw_News_By_Week_Avg_Fields,
  vw_news_by_week_avg_order_by: Vw_News_By_Week_Avg_Order_By,
  vw_news_by_week_bool_exp: Vw_News_By_Week_Bool_Exp,
  vw_news_by_week_max_fields: Vw_News_By_Week_Max_Fields,
  vw_news_by_week_max_order_by: Vw_News_By_Week_Max_Order_By,
  vw_news_by_week_min_fields: Vw_News_By_Week_Min_Fields,
  vw_news_by_week_min_order_by: Vw_News_By_Week_Min_Order_By,
  vw_news_by_week_order_by: Vw_News_By_Week_Order_By,
  vw_news_by_week_select_column: Vw_News_By_Week_Select_Column,
  vw_news_by_week_stddev_fields: Vw_News_By_Week_Stddev_Fields,
  vw_news_by_week_stddev_order_by: Vw_News_By_Week_Stddev_Order_By,
  vw_news_by_week_stddev_pop_fields: Vw_News_By_Week_Stddev_Pop_Fields,
  vw_news_by_week_stddev_pop_order_by: Vw_News_By_Week_Stddev_Pop_Order_By,
  vw_news_by_week_stddev_samp_fields: Vw_News_By_Week_Stddev_Samp_Fields,
  vw_news_by_week_stddev_samp_order_by: Vw_News_By_Week_Stddev_Samp_Order_By,
  vw_news_by_week_sum_fields: Vw_News_By_Week_Sum_Fields,
  vw_news_by_week_sum_order_by: Vw_News_By_Week_Sum_Order_By,
  vw_news_by_week_var_pop_fields: Vw_News_By_Week_Var_Pop_Fields,
  vw_news_by_week_var_pop_order_by: Vw_News_By_Week_Var_Pop_Order_By,
  vw_news_by_week_var_samp_fields: Vw_News_By_Week_Var_Samp_Fields,
  vw_news_by_week_var_samp_order_by: Vw_News_By_Week_Var_Samp_Order_By,
  vw_news_by_week_variance_fields: Vw_News_By_Week_Variance_Fields,
  vw_news_by_week_variance_order_by: Vw_News_By_Week_Variance_Order_By,
  vw_type_name: Vw_Type_Name,
  vw_type_name_aggregate: Vw_Type_Name_Aggregate,
  vw_type_name_aggregate_fields: Vw_Type_Name_Aggregate_Fields,
  vw_type_name_aggregate_order_by: Vw_Type_Name_Aggregate_Order_By,
  vw_type_name_avg_fields: Vw_Type_Name_Avg_Fields,
  vw_type_name_avg_order_by: Vw_Type_Name_Avg_Order_By,
  vw_type_name_bool_exp: Vw_Type_Name_Bool_Exp,
  vw_type_name_max_fields: Vw_Type_Name_Max_Fields,
  vw_type_name_max_order_by: Vw_Type_Name_Max_Order_By,
  vw_type_name_min_fields: Vw_Type_Name_Min_Fields,
  vw_type_name_min_order_by: Vw_Type_Name_Min_Order_By,
  vw_type_name_order_by: Vw_Type_Name_Order_By,
  vw_type_name_select_column: Vw_Type_Name_Select_Column,
  vw_type_name_stddev_fields: Vw_Type_Name_Stddev_Fields,
  vw_type_name_stddev_order_by: Vw_Type_Name_Stddev_Order_By,
  vw_type_name_stddev_pop_fields: Vw_Type_Name_Stddev_Pop_Fields,
  vw_type_name_stddev_pop_order_by: Vw_Type_Name_Stddev_Pop_Order_By,
  vw_type_name_stddev_samp_fields: Vw_Type_Name_Stddev_Samp_Fields,
  vw_type_name_stddev_samp_order_by: Vw_Type_Name_Stddev_Samp_Order_By,
  vw_type_name_sum_fields: Vw_Type_Name_Sum_Fields,
  vw_type_name_sum_order_by: Vw_Type_Name_Sum_Order_By,
  vw_type_name_var_pop_fields: Vw_Type_Name_Var_Pop_Fields,
  vw_type_name_var_pop_order_by: Vw_Type_Name_Var_Pop_Order_By,
  vw_type_name_var_samp_fields: Vw_Type_Name_Var_Samp_Fields,
  vw_type_name_var_samp_order_by: Vw_Type_Name_Var_Samp_Order_By,
  vw_type_name_variance_fields: Vw_Type_Name_Variance_Fields,
  vw_type_name_variance_order_by: Vw_Type_Name_Variance_Order_By,
};

export type AdminsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins'] = ResolversParentTypes['admins']> = {
  full_name?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  password?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_aggregate'] = ResolversParentTypes['admins_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['admins_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['admins']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_aggregate_fields'] = ResolversParentTypes['admins_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['admins_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Admins_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['admins_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['admins_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['admins_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['admins_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['admins_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['admins_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['admins_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['admins_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['admins_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_avg_fields'] = ResolversParentTypes['admins_avg_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_max_fields'] = ResolversParentTypes['admins_max_fields']> = {
  full_name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_min_fields'] = ResolversParentTypes['admins_min_fields']> = {
  full_name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Mutation_ResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_mutation_response'] = ResolversParentTypes['admins_mutation_response']> = {
  affected_rows?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  returning?: Resolver<Array<ResolversTypes['admins']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_stddev_fields'] = ResolversParentTypes['admins_stddev_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_stddev_pop_fields'] = ResolversParentTypes['admins_stddev_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_stddev_samp_fields'] = ResolversParentTypes['admins_stddev_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_sum_fields'] = ResolversParentTypes['admins_sum_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_var_pop_fields'] = ResolversParentTypes['admins_var_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_var_samp_fields'] = ResolversParentTypes['admins_var_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Admins_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['admins_variance_fields'] = ResolversParentTypes['admins_variance_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type AuthorsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors'] = ResolversParentTypes['authors']> = {
  birthdate?: Resolver<Maybe<ResolversTypes['date']>, ParentType, ContextType>,
  created_at?: Resolver<ResolversTypes['timestamptz'], ParentType, ContextType>,
  deleted_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updated_at?: Resolver<ResolversTypes['timestamptz'], ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_aggregate'] = ResolversParentTypes['authors_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['authors_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['authors']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_aggregate_fields'] = ResolversParentTypes['authors_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['authors_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Authors_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['authors_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['authors_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['authors_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['authors_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['authors_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['authors_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['authors_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['authors_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['authors_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_avg_fields'] = ResolversParentTypes['authors_avg_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_max_fields'] = ResolversParentTypes['authors_max_fields']> = {
  birthdate?: Resolver<Maybe<ResolversTypes['date']>, ParentType, ContextType>,
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  deleted_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updated_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_min_fields'] = ResolversParentTypes['authors_min_fields']> = {
  birthdate?: Resolver<Maybe<ResolversTypes['date']>, ParentType, ContextType>,
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  deleted_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  password?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updated_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Mutation_ResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_mutation_response'] = ResolversParentTypes['authors_mutation_response']> = {
  affected_rows?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  returning?: Resolver<Array<ResolversTypes['authors']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_stddev_fields'] = ResolversParentTypes['authors_stddev_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_stddev_pop_fields'] = ResolversParentTypes['authors_stddev_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_stddev_samp_fields'] = ResolversParentTypes['authors_stddev_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_sum_fields'] = ResolversParentTypes['authors_sum_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_var_pop_fields'] = ResolversParentTypes['authors_var_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_var_samp_fields'] = ResolversParentTypes['authors_var_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Authors_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['authors_variance_fields'] = ResolversParentTypes['authors_variance_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type CommentsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments'] = ResolversParentTypes['comments']> = {
  comment?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  created_at?: Resolver<ResolversTypes['timestamptz'], ParentType, ContextType>,
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  news_id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_aggregate'] = ResolversParentTypes['comments_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['comments_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['comments']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_aggregate_fields'] = ResolversParentTypes['comments_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['comments_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Comments_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['comments_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['comments_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['comments_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['comments_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['comments_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['comments_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['comments_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['comments_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['comments_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_avg_fields'] = ResolversParentTypes['comments_avg_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_max_fields'] = ResolversParentTypes['comments_max_fields']> = {
  comment?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_min_fields'] = ResolversParentTypes['comments_min_fields']> = {
  comment?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Mutation_ResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_mutation_response'] = ResolversParentTypes['comments_mutation_response']> = {
  affected_rows?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  returning?: Resolver<Array<ResolversTypes['comments']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_stddev_fields'] = ResolversParentTypes['comments_stddev_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_stddev_pop_fields'] = ResolversParentTypes['comments_stddev_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_stddev_samp_fields'] = ResolversParentTypes['comments_stddev_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_sum_fields'] = ResolversParentTypes['comments_sum_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_var_pop_fields'] = ResolversParentTypes['comments_var_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_var_samp_fields'] = ResolversParentTypes['comments_var_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Comments_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['comments_variance_fields'] = ResolversParentTypes['comments_variance_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export interface DateScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['date'], any> {
  name: 'date'
}

export type Mutation_RootResolvers<ContextType = any, ParentType extends ResolversParentTypes['mutation_root'] = ResolversParentTypes['mutation_root']> = {
  delete_admins?: Resolver<Maybe<ResolversTypes['admins_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootDelete_AdminsArgs, 'where'>>,
  delete_authors?: Resolver<Maybe<ResolversTypes['authors_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootDelete_AuthorsArgs, 'where'>>,
  delete_comments?: Resolver<Maybe<ResolversTypes['comments_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootDelete_CommentsArgs, 'where'>>,
  delete_news?: Resolver<Maybe<ResolversTypes['news_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootDelete_NewsArgs, 'where'>>,
  delete_reactions?: Resolver<Maybe<ResolversTypes['reactions_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootDelete_ReactionsArgs, 'where'>>,
  insert_admins?: Resolver<Maybe<ResolversTypes['admins_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootInsert_AdminsArgs, 'objects'>>,
  insert_authors?: Resolver<Maybe<ResolversTypes['authors_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootInsert_AuthorsArgs, 'objects'>>,
  insert_comments?: Resolver<Maybe<ResolversTypes['comments_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootInsert_CommentsArgs, 'objects'>>,
  insert_news?: Resolver<Maybe<ResolversTypes['news_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootInsert_NewsArgs, 'objects'>>,
  insert_reactions?: Resolver<Maybe<ResolversTypes['reactions_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootInsert_ReactionsArgs, 'objects'>>,
  update_admins?: Resolver<Maybe<ResolversTypes['admins_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootUpdate_AdminsArgs, 'where'>>,
  update_authors?: Resolver<Maybe<ResolversTypes['authors_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootUpdate_AuthorsArgs, 'where'>>,
  update_comments?: Resolver<Maybe<ResolversTypes['comments_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootUpdate_CommentsArgs, 'where'>>,
  update_news?: Resolver<Maybe<ResolversTypes['news_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootUpdate_NewsArgs, 'where'>>,
  update_reactions?: Resolver<Maybe<ResolversTypes['reactions_mutation_response']>, ParentType, ContextType, RequireFields<Mutation_RootUpdate_ReactionsArgs, 'where'>>,
};

export type NewsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news'] = ResolversParentTypes['news']> = {
  author_id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  created_at?: Resolver<ResolversTypes['timestamptz'], ParentType, ContextType>,
  deleted_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  news_detail?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  type?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  updated_at?: Resolver<ResolversTypes['timestamptz'], ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_aggregate'] = ResolversParentTypes['news_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['news_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['news']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_aggregate_fields'] = ResolversParentTypes['news_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['news_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<News_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['news_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['news_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['news_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['news_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['news_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['news_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['news_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['news_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['news_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_avg_fields'] = ResolversParentTypes['news_avg_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_max_fields'] = ResolversParentTypes['news_max_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  deleted_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  news_detail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updated_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_min_fields'] = ResolversParentTypes['news_min_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  deleted_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  news_detail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updated_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Mutation_ResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_mutation_response'] = ResolversParentTypes['news_mutation_response']> = {
  affected_rows?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  returning?: Resolver<Array<ResolversTypes['news']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_stddev_fields'] = ResolversParentTypes['news_stddev_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_stddev_pop_fields'] = ResolversParentTypes['news_stddev_pop_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_stddev_samp_fields'] = ResolversParentTypes['news_stddev_samp_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_sum_fields'] = ResolversParentTypes['news_sum_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_var_pop_fields'] = ResolversParentTypes['news_var_pop_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_var_samp_fields'] = ResolversParentTypes['news_var_samp_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type News_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['news_variance_fields'] = ResolversParentTypes['news_variance_fields']> = {
  author_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Query_RootResolvers<ContextType = any, ParentType extends ResolversParentTypes['query_root'] = ResolversParentTypes['query_root']> = {
  admins?: Resolver<Array<ResolversTypes['admins']>, ParentType, ContextType, RequireFields<Query_RootAdminsArgs, never>>,
  admins_aggregate?: Resolver<ResolversTypes['admins_aggregate'], ParentType, ContextType, RequireFields<Query_RootAdmins_AggregateArgs, never>>,
  admins_by_pk?: Resolver<Maybe<ResolversTypes['admins']>, ParentType, ContextType, RequireFields<Query_RootAdmins_By_PkArgs, 'id'>>,
  authors?: Resolver<Array<ResolversTypes['authors']>, ParentType, ContextType, RequireFields<Query_RootAuthorsArgs, never>>,
  authors_aggregate?: Resolver<ResolversTypes['authors_aggregate'], ParentType, ContextType, RequireFields<Query_RootAuthors_AggregateArgs, never>>,
  authors_by_pk?: Resolver<Maybe<ResolversTypes['authors']>, ParentType, ContextType, RequireFields<Query_RootAuthors_By_PkArgs, 'id'>>,
  comments?: Resolver<Array<ResolversTypes['comments']>, ParentType, ContextType, RequireFields<Query_RootCommentsArgs, never>>,
  comments_aggregate?: Resolver<ResolversTypes['comments_aggregate'], ParentType, ContextType, RequireFields<Query_RootComments_AggregateArgs, never>>,
  comments_by_pk?: Resolver<Maybe<ResolversTypes['comments']>, ParentType, ContextType, RequireFields<Query_RootComments_By_PkArgs, 'id'>>,
  news?: Resolver<Array<ResolversTypes['news']>, ParentType, ContextType, RequireFields<Query_RootNewsArgs, never>>,
  news_aggregate?: Resolver<ResolversTypes['news_aggregate'], ParentType, ContextType, RequireFields<Query_RootNews_AggregateArgs, never>>,
  news_by_pk?: Resolver<Maybe<ResolversTypes['news']>, ParentType, ContextType, RequireFields<Query_RootNews_By_PkArgs, 'id'>>,
  reactions?: Resolver<Array<ResolversTypes['reactions']>, ParentType, ContextType, RequireFields<Query_RootReactionsArgs, never>>,
  reactions_aggregate?: Resolver<ResolversTypes['reactions_aggregate'], ParentType, ContextType, RequireFields<Query_RootReactions_AggregateArgs, never>>,
  reactions_by_pk?: Resolver<Maybe<ResolversTypes['reactions']>, ParentType, ContextType, RequireFields<Query_RootReactions_By_PkArgs, 'id'>>,
  vw_detail_news?: Resolver<Array<ResolversTypes['vw_detail_news']>, ParentType, ContextType, RequireFields<Query_RootVw_Detail_NewsArgs, never>>,
  vw_detail_news_aggregate?: Resolver<ResolversTypes['vw_detail_news_aggregate'], ParentType, ContextType, RequireFields<Query_RootVw_Detail_News_AggregateArgs, never>>,
  vw_news_by_week?: Resolver<Array<ResolversTypes['vw_news_by_week']>, ParentType, ContextType, RequireFields<Query_RootVw_News_By_WeekArgs, never>>,
  vw_news_by_week_aggregate?: Resolver<ResolversTypes['vw_news_by_week_aggregate'], ParentType, ContextType, RequireFields<Query_RootVw_News_By_Week_AggregateArgs, never>>,
  vw_type_name?: Resolver<Array<ResolversTypes['vw_type_name']>, ParentType, ContextType, RequireFields<Query_RootVw_Type_NameArgs, never>>,
  vw_type_name_aggregate?: Resolver<ResolversTypes['vw_type_name_aggregate'], ParentType, ContextType, RequireFields<Query_RootVw_Type_Name_AggregateArgs, never>>,
};

export type ReactionsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions'] = ResolversParentTypes['reactions']> = {
  count?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  news_id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_aggregate'] = ResolversParentTypes['reactions_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['reactions_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['reactions']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_aggregate_fields'] = ResolversParentTypes['reactions_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['reactions_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Reactions_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['reactions_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['reactions_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['reactions_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['reactions_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['reactions_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['reactions_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['reactions_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['reactions_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['reactions_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_avg_fields'] = ResolversParentTypes['reactions_avg_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_max_fields'] = ResolversParentTypes['reactions_max_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_min_fields'] = ResolversParentTypes['reactions_min_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Mutation_ResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_mutation_response'] = ResolversParentTypes['reactions_mutation_response']> = {
  affected_rows?: Resolver<ResolversTypes['Int'], ParentType, ContextType>,
  returning?: Resolver<Array<ResolversTypes['reactions']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_stddev_fields'] = ResolversParentTypes['reactions_stddev_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_stddev_pop_fields'] = ResolversParentTypes['reactions_stddev_pop_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_stddev_samp_fields'] = ResolversParentTypes['reactions_stddev_samp_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_sum_fields'] = ResolversParentTypes['reactions_sum_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_var_pop_fields'] = ResolversParentTypes['reactions_var_pop_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_var_samp_fields'] = ResolversParentTypes['reactions_var_samp_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Reactions_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['reactions_variance_fields'] = ResolversParentTypes['reactions_variance_fields']> = {
  count?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  news_id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Subscription_RootResolvers<ContextType = any, ParentType extends ResolversParentTypes['subscription_root'] = ResolversParentTypes['subscription_root']> = {
  admins?: SubscriptionResolver<Array<ResolversTypes['admins']>, "admins", ParentType, ContextType, RequireFields<Subscription_RootAdminsArgs, never>>,
  admins_aggregate?: SubscriptionResolver<ResolversTypes['admins_aggregate'], "admins_aggregate", ParentType, ContextType, RequireFields<Subscription_RootAdmins_AggregateArgs, never>>,
  admins_by_pk?: SubscriptionResolver<Maybe<ResolversTypes['admins']>, "admins_by_pk", ParentType, ContextType, RequireFields<Subscription_RootAdmins_By_PkArgs, 'id'>>,
  authors?: SubscriptionResolver<Array<ResolversTypes['authors']>, "authors", ParentType, ContextType, RequireFields<Subscription_RootAuthorsArgs, never>>,
  authors_aggregate?: SubscriptionResolver<ResolversTypes['authors_aggregate'], "authors_aggregate", ParentType, ContextType, RequireFields<Subscription_RootAuthors_AggregateArgs, never>>,
  authors_by_pk?: SubscriptionResolver<Maybe<ResolversTypes['authors']>, "authors_by_pk", ParentType, ContextType, RequireFields<Subscription_RootAuthors_By_PkArgs, 'id'>>,
  comments?: SubscriptionResolver<Array<ResolversTypes['comments']>, "comments", ParentType, ContextType, RequireFields<Subscription_RootCommentsArgs, never>>,
  comments_aggregate?: SubscriptionResolver<ResolversTypes['comments_aggregate'], "comments_aggregate", ParentType, ContextType, RequireFields<Subscription_RootComments_AggregateArgs, never>>,
  comments_by_pk?: SubscriptionResolver<Maybe<ResolversTypes['comments']>, "comments_by_pk", ParentType, ContextType, RequireFields<Subscription_RootComments_By_PkArgs, 'id'>>,
  news?: SubscriptionResolver<Array<ResolversTypes['news']>, "news", ParentType, ContextType, RequireFields<Subscription_RootNewsArgs, never>>,
  news_aggregate?: SubscriptionResolver<ResolversTypes['news_aggregate'], "news_aggregate", ParentType, ContextType, RequireFields<Subscription_RootNews_AggregateArgs, never>>,
  news_by_pk?: SubscriptionResolver<Maybe<ResolversTypes['news']>, "news_by_pk", ParentType, ContextType, RequireFields<Subscription_RootNews_By_PkArgs, 'id'>>,
  reactions?: SubscriptionResolver<Array<ResolversTypes['reactions']>, "reactions", ParentType, ContextType, RequireFields<Subscription_RootReactionsArgs, never>>,
  reactions_aggregate?: SubscriptionResolver<ResolversTypes['reactions_aggregate'], "reactions_aggregate", ParentType, ContextType, RequireFields<Subscription_RootReactions_AggregateArgs, never>>,
  reactions_by_pk?: SubscriptionResolver<Maybe<ResolversTypes['reactions']>, "reactions_by_pk", ParentType, ContextType, RequireFields<Subscription_RootReactions_By_PkArgs, 'id'>>,
  vw_detail_news?: SubscriptionResolver<Array<ResolversTypes['vw_detail_news']>, "vw_detail_news", ParentType, ContextType, RequireFields<Subscription_RootVw_Detail_NewsArgs, never>>,
  vw_detail_news_aggregate?: SubscriptionResolver<ResolversTypes['vw_detail_news_aggregate'], "vw_detail_news_aggregate", ParentType, ContextType, RequireFields<Subscription_RootVw_Detail_News_AggregateArgs, never>>,
  vw_news_by_week?: SubscriptionResolver<Array<ResolversTypes['vw_news_by_week']>, "vw_news_by_week", ParentType, ContextType, RequireFields<Subscription_RootVw_News_By_WeekArgs, never>>,
  vw_news_by_week_aggregate?: SubscriptionResolver<ResolversTypes['vw_news_by_week_aggregate'], "vw_news_by_week_aggregate", ParentType, ContextType, RequireFields<Subscription_RootVw_News_By_Week_AggregateArgs, never>>,
  vw_type_name?: SubscriptionResolver<Array<ResolversTypes['vw_type_name']>, "vw_type_name", ParentType, ContextType, RequireFields<Subscription_RootVw_Type_NameArgs, never>>,
  vw_type_name_aggregate?: SubscriptionResolver<ResolversTypes['vw_type_name_aggregate'], "vw_type_name_aggregate", ParentType, ContextType, RequireFields<Subscription_RootVw_Type_Name_AggregateArgs, never>>,
};

export interface TimestamptzScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['timestamptz'], any> {
  name: 'timestamptz'
}

export type Vw_Detail_NewsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news'] = ResolversParentTypes['vw_detail_news']> = {
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  news_detail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_aggregate'] = ResolversParentTypes['vw_detail_news_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['vw_detail_news_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['vw_detail_news']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_aggregate_fields'] = ResolversParentTypes['vw_detail_news_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['vw_detail_news_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Vw_Detail_News_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['vw_detail_news_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['vw_detail_news_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['vw_detail_news_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['vw_detail_news_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['vw_detail_news_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['vw_detail_news_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['vw_detail_news_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['vw_detail_news_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['vw_detail_news_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_avg_fields'] = ResolversParentTypes['vw_detail_news_avg_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_max_fields'] = ResolversParentTypes['vw_detail_news_max_fields']> = {
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  news_detail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_min_fields'] = ResolversParentTypes['vw_detail_news_min_fields']> = {
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  news_detail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_stddev_fields'] = ResolversParentTypes['vw_detail_news_stddev_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_stddev_pop_fields'] = ResolversParentTypes['vw_detail_news_stddev_pop_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_stddev_samp_fields'] = ResolversParentTypes['vw_detail_news_stddev_samp_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_sum_fields'] = ResolversParentTypes['vw_detail_news_sum_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_var_pop_fields'] = ResolversParentTypes['vw_detail_news_var_pop_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_var_samp_fields'] = ResolversParentTypes['vw_detail_news_var_samp_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Detail_News_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_detail_news_variance_fields'] = ResolversParentTypes['vw_detail_news_variance_fields']> = {
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_WeekResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week'] = ResolversParentTypes['vw_news_by_week']> = {
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_aggregate'] = ResolversParentTypes['vw_news_by_week_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['vw_news_by_week_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['vw_news_by_week']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_aggregate_fields'] = ResolversParentTypes['vw_news_by_week_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['vw_news_by_week_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Vw_News_By_Week_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['vw_news_by_week_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['vw_news_by_week_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['vw_news_by_week_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['vw_news_by_week_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['vw_news_by_week_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['vw_news_by_week_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['vw_news_by_week_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['vw_news_by_week_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['vw_news_by_week_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_avg_fields'] = ResolversParentTypes['vw_news_by_week_avg_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_max_fields'] = ResolversParentTypes['vw_news_by_week_max_fields']> = {
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_min_fields'] = ResolversParentTypes['vw_news_by_week_min_fields']> = {
  created_at?: Resolver<Maybe<ResolversTypes['timestamptz']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  img_url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_stddev_fields'] = ResolversParentTypes['vw_news_by_week_stddev_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_stddev_pop_fields'] = ResolversParentTypes['vw_news_by_week_stddev_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_stddev_samp_fields'] = ResolversParentTypes['vw_news_by_week_stddev_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_sum_fields'] = ResolversParentTypes['vw_news_by_week_sum_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_var_pop_fields'] = ResolversParentTypes['vw_news_by_week_var_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_var_samp_fields'] = ResolversParentTypes['vw_news_by_week_var_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_News_By_Week_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_news_by_week_variance_fields'] = ResolversParentTypes['vw_news_by_week_variance_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  views?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_NameResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name'] = ResolversParentTypes['vw_type_name']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_AggregateResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_aggregate'] = ResolversParentTypes['vw_type_name_aggregate']> = {
  aggregate?: Resolver<Maybe<ResolversTypes['vw_type_name_aggregate_fields']>, ParentType, ContextType>,
  nodes?: Resolver<Array<ResolversTypes['vw_type_name']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Aggregate_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_aggregate_fields'] = ResolversParentTypes['vw_type_name_aggregate_fields']> = {
  avg?: Resolver<Maybe<ResolversTypes['vw_type_name_avg_fields']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType, RequireFields<Vw_Type_Name_Aggregate_FieldsCountArgs, never>>,
  max?: Resolver<Maybe<ResolversTypes['vw_type_name_max_fields']>, ParentType, ContextType>,
  min?: Resolver<Maybe<ResolversTypes['vw_type_name_min_fields']>, ParentType, ContextType>,
  stddev?: Resolver<Maybe<ResolversTypes['vw_type_name_stddev_fields']>, ParentType, ContextType>,
  stddev_pop?: Resolver<Maybe<ResolversTypes['vw_type_name_stddev_pop_fields']>, ParentType, ContextType>,
  stddev_samp?: Resolver<Maybe<ResolversTypes['vw_type_name_stddev_samp_fields']>, ParentType, ContextType>,
  sum?: Resolver<Maybe<ResolversTypes['vw_type_name_sum_fields']>, ParentType, ContextType>,
  var_pop?: Resolver<Maybe<ResolversTypes['vw_type_name_var_pop_fields']>, ParentType, ContextType>,
  var_samp?: Resolver<Maybe<ResolversTypes['vw_type_name_var_samp_fields']>, ParentType, ContextType>,
  variance?: Resolver<Maybe<ResolversTypes['vw_type_name_variance_fields']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Avg_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_avg_fields'] = ResolversParentTypes['vw_type_name_avg_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Max_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_max_fields'] = ResolversParentTypes['vw_type_name_max_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Min_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_min_fields'] = ResolversParentTypes['vw_type_name_min_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Stddev_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_stddev_fields'] = ResolversParentTypes['vw_type_name_stddev_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Stddev_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_stddev_pop_fields'] = ResolversParentTypes['vw_type_name_stddev_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Stddev_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_stddev_samp_fields'] = ResolversParentTypes['vw_type_name_stddev_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Sum_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_sum_fields'] = ResolversParentTypes['vw_type_name_sum_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Var_Pop_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_var_pop_fields'] = ResolversParentTypes['vw_type_name_var_pop_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Var_Samp_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_var_samp_fields'] = ResolversParentTypes['vw_type_name_var_samp_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Vw_Type_Name_Variance_FieldsResolvers<ContextType = any, ParentType extends ResolversParentTypes['vw_type_name_variance_fields'] = ResolversParentTypes['vw_type_name_variance_fields']> = {
  id?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type Resolvers<ContextType = any> = {
  admins?: AdminsResolvers<ContextType>,
  admins_aggregate?: Admins_AggregateResolvers<ContextType>,
  admins_aggregate_fields?: Admins_Aggregate_FieldsResolvers<ContextType>,
  admins_avg_fields?: Admins_Avg_FieldsResolvers<ContextType>,
  admins_max_fields?: Admins_Max_FieldsResolvers<ContextType>,
  admins_min_fields?: Admins_Min_FieldsResolvers<ContextType>,
  admins_mutation_response?: Admins_Mutation_ResponseResolvers<ContextType>,
  admins_stddev_fields?: Admins_Stddev_FieldsResolvers<ContextType>,
  admins_stddev_pop_fields?: Admins_Stddev_Pop_FieldsResolvers<ContextType>,
  admins_stddev_samp_fields?: Admins_Stddev_Samp_FieldsResolvers<ContextType>,
  admins_sum_fields?: Admins_Sum_FieldsResolvers<ContextType>,
  admins_var_pop_fields?: Admins_Var_Pop_FieldsResolvers<ContextType>,
  admins_var_samp_fields?: Admins_Var_Samp_FieldsResolvers<ContextType>,
  admins_variance_fields?: Admins_Variance_FieldsResolvers<ContextType>,
  authors?: AuthorsResolvers<ContextType>,
  authors_aggregate?: Authors_AggregateResolvers<ContextType>,
  authors_aggregate_fields?: Authors_Aggregate_FieldsResolvers<ContextType>,
  authors_avg_fields?: Authors_Avg_FieldsResolvers<ContextType>,
  authors_max_fields?: Authors_Max_FieldsResolvers<ContextType>,
  authors_min_fields?: Authors_Min_FieldsResolvers<ContextType>,
  authors_mutation_response?: Authors_Mutation_ResponseResolvers<ContextType>,
  authors_stddev_fields?: Authors_Stddev_FieldsResolvers<ContextType>,
  authors_stddev_pop_fields?: Authors_Stddev_Pop_FieldsResolvers<ContextType>,
  authors_stddev_samp_fields?: Authors_Stddev_Samp_FieldsResolvers<ContextType>,
  authors_sum_fields?: Authors_Sum_FieldsResolvers<ContextType>,
  authors_var_pop_fields?: Authors_Var_Pop_FieldsResolvers<ContextType>,
  authors_var_samp_fields?: Authors_Var_Samp_FieldsResolvers<ContextType>,
  authors_variance_fields?: Authors_Variance_FieldsResolvers<ContextType>,
  comments?: CommentsResolvers<ContextType>,
  comments_aggregate?: Comments_AggregateResolvers<ContextType>,
  comments_aggregate_fields?: Comments_Aggregate_FieldsResolvers<ContextType>,
  comments_avg_fields?: Comments_Avg_FieldsResolvers<ContextType>,
  comments_max_fields?: Comments_Max_FieldsResolvers<ContextType>,
  comments_min_fields?: Comments_Min_FieldsResolvers<ContextType>,
  comments_mutation_response?: Comments_Mutation_ResponseResolvers<ContextType>,
  comments_stddev_fields?: Comments_Stddev_FieldsResolvers<ContextType>,
  comments_stddev_pop_fields?: Comments_Stddev_Pop_FieldsResolvers<ContextType>,
  comments_stddev_samp_fields?: Comments_Stddev_Samp_FieldsResolvers<ContextType>,
  comments_sum_fields?: Comments_Sum_FieldsResolvers<ContextType>,
  comments_var_pop_fields?: Comments_Var_Pop_FieldsResolvers<ContextType>,
  comments_var_samp_fields?: Comments_Var_Samp_FieldsResolvers<ContextType>,
  comments_variance_fields?: Comments_Variance_FieldsResolvers<ContextType>,
  date?: GraphQLScalarType,
  mutation_root?: Mutation_RootResolvers<ContextType>,
  news?: NewsResolvers<ContextType>,
  news_aggregate?: News_AggregateResolvers<ContextType>,
  news_aggregate_fields?: News_Aggregate_FieldsResolvers<ContextType>,
  news_avg_fields?: News_Avg_FieldsResolvers<ContextType>,
  news_max_fields?: News_Max_FieldsResolvers<ContextType>,
  news_min_fields?: News_Min_FieldsResolvers<ContextType>,
  news_mutation_response?: News_Mutation_ResponseResolvers<ContextType>,
  news_stddev_fields?: News_Stddev_FieldsResolvers<ContextType>,
  news_stddev_pop_fields?: News_Stddev_Pop_FieldsResolvers<ContextType>,
  news_stddev_samp_fields?: News_Stddev_Samp_FieldsResolvers<ContextType>,
  news_sum_fields?: News_Sum_FieldsResolvers<ContextType>,
  news_var_pop_fields?: News_Var_Pop_FieldsResolvers<ContextType>,
  news_var_samp_fields?: News_Var_Samp_FieldsResolvers<ContextType>,
  news_variance_fields?: News_Variance_FieldsResolvers<ContextType>,
  query_root?: Query_RootResolvers<ContextType>,
  reactions?: ReactionsResolvers<ContextType>,
  reactions_aggregate?: Reactions_AggregateResolvers<ContextType>,
  reactions_aggregate_fields?: Reactions_Aggregate_FieldsResolvers<ContextType>,
  reactions_avg_fields?: Reactions_Avg_FieldsResolvers<ContextType>,
  reactions_max_fields?: Reactions_Max_FieldsResolvers<ContextType>,
  reactions_min_fields?: Reactions_Min_FieldsResolvers<ContextType>,
  reactions_mutation_response?: Reactions_Mutation_ResponseResolvers<ContextType>,
  reactions_stddev_fields?: Reactions_Stddev_FieldsResolvers<ContextType>,
  reactions_stddev_pop_fields?: Reactions_Stddev_Pop_FieldsResolvers<ContextType>,
  reactions_stddev_samp_fields?: Reactions_Stddev_Samp_FieldsResolvers<ContextType>,
  reactions_sum_fields?: Reactions_Sum_FieldsResolvers<ContextType>,
  reactions_var_pop_fields?: Reactions_Var_Pop_FieldsResolvers<ContextType>,
  reactions_var_samp_fields?: Reactions_Var_Samp_FieldsResolvers<ContextType>,
  reactions_variance_fields?: Reactions_Variance_FieldsResolvers<ContextType>,
  subscription_root?: Subscription_RootResolvers<ContextType>,
  timestamptz?: GraphQLScalarType,
  vw_detail_news?: Vw_Detail_NewsResolvers<ContextType>,
  vw_detail_news_aggregate?: Vw_Detail_News_AggregateResolvers<ContextType>,
  vw_detail_news_aggregate_fields?: Vw_Detail_News_Aggregate_FieldsResolvers<ContextType>,
  vw_detail_news_avg_fields?: Vw_Detail_News_Avg_FieldsResolvers<ContextType>,
  vw_detail_news_max_fields?: Vw_Detail_News_Max_FieldsResolvers<ContextType>,
  vw_detail_news_min_fields?: Vw_Detail_News_Min_FieldsResolvers<ContextType>,
  vw_detail_news_stddev_fields?: Vw_Detail_News_Stddev_FieldsResolvers<ContextType>,
  vw_detail_news_stddev_pop_fields?: Vw_Detail_News_Stddev_Pop_FieldsResolvers<ContextType>,
  vw_detail_news_stddev_samp_fields?: Vw_Detail_News_Stddev_Samp_FieldsResolvers<ContextType>,
  vw_detail_news_sum_fields?: Vw_Detail_News_Sum_FieldsResolvers<ContextType>,
  vw_detail_news_var_pop_fields?: Vw_Detail_News_Var_Pop_FieldsResolvers<ContextType>,
  vw_detail_news_var_samp_fields?: Vw_Detail_News_Var_Samp_FieldsResolvers<ContextType>,
  vw_detail_news_variance_fields?: Vw_Detail_News_Variance_FieldsResolvers<ContextType>,
  vw_news_by_week?: Vw_News_By_WeekResolvers<ContextType>,
  vw_news_by_week_aggregate?: Vw_News_By_Week_AggregateResolvers<ContextType>,
  vw_news_by_week_aggregate_fields?: Vw_News_By_Week_Aggregate_FieldsResolvers<ContextType>,
  vw_news_by_week_avg_fields?: Vw_News_By_Week_Avg_FieldsResolvers<ContextType>,
  vw_news_by_week_max_fields?: Vw_News_By_Week_Max_FieldsResolvers<ContextType>,
  vw_news_by_week_min_fields?: Vw_News_By_Week_Min_FieldsResolvers<ContextType>,
  vw_news_by_week_stddev_fields?: Vw_News_By_Week_Stddev_FieldsResolvers<ContextType>,
  vw_news_by_week_stddev_pop_fields?: Vw_News_By_Week_Stddev_Pop_FieldsResolvers<ContextType>,
  vw_news_by_week_stddev_samp_fields?: Vw_News_By_Week_Stddev_Samp_FieldsResolvers<ContextType>,
  vw_news_by_week_sum_fields?: Vw_News_By_Week_Sum_FieldsResolvers<ContextType>,
  vw_news_by_week_var_pop_fields?: Vw_News_By_Week_Var_Pop_FieldsResolvers<ContextType>,
  vw_news_by_week_var_samp_fields?: Vw_News_By_Week_Var_Samp_FieldsResolvers<ContextType>,
  vw_news_by_week_variance_fields?: Vw_News_By_Week_Variance_FieldsResolvers<ContextType>,
  vw_type_name?: Vw_Type_NameResolvers<ContextType>,
  vw_type_name_aggregate?: Vw_Type_Name_AggregateResolvers<ContextType>,
  vw_type_name_aggregate_fields?: Vw_Type_Name_Aggregate_FieldsResolvers<ContextType>,
  vw_type_name_avg_fields?: Vw_Type_Name_Avg_FieldsResolvers<ContextType>,
  vw_type_name_max_fields?: Vw_Type_Name_Max_FieldsResolvers<ContextType>,
  vw_type_name_min_fields?: Vw_Type_Name_Min_FieldsResolvers<ContextType>,
  vw_type_name_stddev_fields?: Vw_Type_Name_Stddev_FieldsResolvers<ContextType>,
  vw_type_name_stddev_pop_fields?: Vw_Type_Name_Stddev_Pop_FieldsResolvers<ContextType>,
  vw_type_name_stddev_samp_fields?: Vw_Type_Name_Stddev_Samp_FieldsResolvers<ContextType>,
  vw_type_name_sum_fields?: Vw_Type_Name_Sum_FieldsResolvers<ContextType>,
  vw_type_name_var_pop_fields?: Vw_Type_Name_Var_Pop_FieldsResolvers<ContextType>,
  vw_type_name_var_samp_fields?: Vw_Type_Name_Var_Samp_FieldsResolvers<ContextType>,
  vw_type_name_variance_fields?: Vw_Type_Name_Variance_FieldsResolvers<ContextType>,
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;


declare module '*/news.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const GET_NEWS: DocumentNode;
export const AUTHORS: DocumentNode;

  export default defaultDocument;
}
    

      export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
      const result: IntrospectionResultData = {
  "__schema": {
    "types": []
  }
};
      export default result;
    