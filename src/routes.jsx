import * as React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient from "apollo-boost";
import "../src/style.css";

//components
import Header from "./Components/Header/header";
import Home from "./Components/Home/home.tsx";
import Footer from "./Components/Footer/footer.tsx";
import Basketball from "./Components/Sports/Basketball/basketball.tsx";
import Judo from "./Components/Sports/Judo/judo.tsx";
import Volleyball from "./Components/Sports/Volleyball/volleyball.tsx";
import Football from "./Components/Sports/Football/football";
import Chess from "./Components/Sports/Chess/chess";
import Container from "../src/Components/Container";
import Admin from "./Components/Admin/admin.tsx";
import Undes from "./Components/Sports/Undes/undes";

const client = new ApolloClient({
  uri: "https://sportsmn.herokuapp.com/v1/graphql",
});

const Routes = () => {
  return (
    <ApolloProvider client={client}>
      <Container>
        <div className="routes_container">
          <Router>
            <Header />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/basketball" component={Basketball} />
              <Route path="/football" component={Football} />
              <Route path="/judo" component={Judo} />
              <Route path="/volleyball" component={Volleyball} />
              <Route path="/chess" component={Chess} />
              <Route path="/admin" component={Admin} />
              <Route path="/undes" component={Undes} />
            </Switch>
            <Footer />
          </Router>
        </div>
      </Container>
    </ApolloProvider>
  );
};

export default Routes;
