import * as React from "react";
import { Carousel, BackTop, Row, Col, Result, Button, Spin } from "antd";
import "antd/dist/antd.css";
import "./home.css";
import { UpCircleFilled } from "@ant-design/icons";
//components
import News from "../News/news";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import useMedia from "use-media";
import Container from "../Container";
import RecentNews from "../Recent_News/recent";
import VideoNews from "../VideoNews";
import Results from "../gameResult/results";

const GET_NEWS = gql`
  {
    vw_news_by_week {
      created_at
      description
      name
      id
      title
      type
      img_url
    }
  }
`;

const Home = () => {
  const isTablet = useMedia({ maxWidth: 1023 });

  const { data, loading, error } = useQuery(GET_NEWS);
  if (loading) return <Spin />;
  if (error)
    return (
      <div>
        <Result
          status="500"
          title="500"
          subTitle="Sorry, Backend has fallen:((."
          extra={<Button type="primary">Back Home</Button>}
        />
        ,
      </div>
    );
  console.log(data && data.vw_news_by_week);

  return (
    <Container>
      <Row style={{ marginTop: "10px" }}>
        <Col md={16} sm={24}>
          <div className="home_header">
            <Carousel autoplay>
              <a href="https://www.nba.com">
                <img
                  src="https://www.nba.com/images/cms/2020-02/2020_All-Star_ChicagoWeek_T1_5.jpg?cw=1045&w=1115&x=70&ch=588&h=627&y=0"
                  alt="wrf"
                />
              </a>
              <a href="https://www.facebook.com/mongolianbasketballassociation/">
                <img
                  src="https://www.nba.com/images/cms/2020-02/anunoby-wolves.jpg?cw=1045&w=1045&x=0&ch=588&h=1568&y=291"
                  alt="GIVE"
                />
              </a>
              <a href="https://www.facebook.com/volleyball.mgl/">
                <img
                  src="https://www.nba.com/images/cms/2020-02/nuggets-celebration-monte-morris.jpg?cw=1045&w=1044&x=0&ch=588&h=695&y=44"
                  alt="GIVE"
                />
              </a>
              <a href="https://www.facebook.com/mongolianchessfederation/">
                <img
                  src="https://www.nba.com/images/cms/2020-02/anthony-davis-suns.jpg?cw=1045&w=1046&x=0&ch=588&h=698&y=36"
                  alt="GIVE"
                />
              </a>
            </Carousel>
          </div>
        </Col>
        <Col
          md={8}
          sm={24}
          style={{ justifyContent: "center", alignItems: "center" }}
        >
          <Col
            md={24}
            style={{
              display: "flex",
              height: "30%",
              justifyContent: "center",
              alignItems: "center ",
              backgroundColor: "#e8e8e8",
              borderRadius: "20px",
              margin: "10px 0 0 10px",
            }}
          >
            <h3>Mongolian Basketball Association</h3>
          </Col>
          <Col
            md={24}
            style={{
              display: "flex",
              height: "30%",
              justifyContent: "center",
              alignItems: "center ",
              backgroundColor: "#e8e8e8",
              borderRadius: "20px",
              margin: "10px 0 0 10px",
            }}
          >
            <h3>Mongolian Basketball Association</h3>
          </Col>
          <Col
            // className="companion"
            md={24}
            style={{
              display: "flex",
              height: "30%",
              justifyContent: "center",
              alignItems: "center ",
              backgroundColor: "#e8e8e8",
              borderRadius: "20px",
              margin: "10px 0 0 10px",
            }}
          >
            <h3>Mongolian Basketball Association</h3>
          </Col>
        </Col>
      </Row>
      <BackTop>
        <div
          style={{
            height: 40,
            width: 40,
            lineHeight: "40px",
            borderRadius: 4,
            backgroundColor: "#1088e9",
            color: "#fff",
            textAlign: "center",
            fontSize: 14,
          }}
        >
          <>
            <UpCircleFilled />
            Дээш
          </>
        </div>
      </BackTop>
      <div>
        <Row>
          <Col md={16}>
            {!loading &&
              data &&
              data.vw_news_by_week.map((vw_news_by_week: any) => (
                <News key={data.id} {...vw_news_by_week} />
              ))}
          </Col>
          <Col md={8}>
            <Results />
          </Col>
        </Row>
      </div>
      {/* <RecentNews /> */}
      {/* <VideoNews /> */}
    </Container>
  );
};

export default Home;
