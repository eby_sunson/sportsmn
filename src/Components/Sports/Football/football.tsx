import * as React from "react";
import { gql } from "apollo-boost";
import News from "../../News/news";
import { useQuery } from "@apollo/react-hooks";

const GET_NEWS = gql`
  {
    get_news {
      created_at
      description
      f_name
      l_name
      title
      type
    }
  }
`;

const Football = () => {
  const { data, loading } = useQuery(GET_NEWS);

  return (
    <div>
      {loading && "Loading..."}
      {!loading &&
        data &&
        data.get_news.map((get_news: any) => <News {...get_news} />)}
    </div>
  );
};
export default Football;
