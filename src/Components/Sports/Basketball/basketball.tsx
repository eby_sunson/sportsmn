import * as React from "react";
import { gql } from "apollo-boost";
import Container from "../../Container";
import { Col, Row, Spin, Result, Button, BackTop } from "antd";
import { useQuery } from "@apollo/react-hooks";
import NewsDetail from "../../NewsDetail/NewsDetail";
import { UpCircleFilled } from "@ant-design/icons";

const GET_NEWS = gql`
  {
    vw_detail_news {
      created_at
      description
      img_url
      name
      news_detail
      title
      type
      views
    }
  }
`;

const Basketball = () => {
  const { data, loading, error } = useQuery(GET_NEWS);
  if (loading) return <Spin />;
  if (error)
    return (
      <div>
        <Result
          status="500"
          title="500"
          subTitle="Sorry, Backend has fallen:((."
          extra={<Button type="primary">Back Home</Button>}
        />
        ,
      </div>
    );
  return (
    <Container>
      {data.vw_detail_news.map((vw_detail_news: any) => (
        <NewsDetail key={data.id} {...vw_detail_news} />
      ))}
      <BackTop>
        <div
          style={{
            height: 40,
            width: 40,  
            lineHeight: "40px",
            borderRadius: 4,
            backgroundColor: "#1088e9",
            color: "#fff",
            textAlign: "center",
            fontSize: 14,
          }}
        >
          <>
            <UpCircleFilled />
            Дээш
          </>
        </div>
      </BackTop>
    </Container>
  );
};

export default Basketball;
