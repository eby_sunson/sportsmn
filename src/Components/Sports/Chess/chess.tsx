import * as React from "react";
import ChessNews from "../../News/news";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";
import { Spin, Result, Button, Row, Col } from "antd";
import TypeMenu from "../../SportByType/TypeMenu";
import SportByType from "../../SportByType/SportByType";

const GET_NEWS = gql`
  {
    vw_detail_news(where: { type: { _eq: "chess" } }) {
      created_at
      description
      img_url
      name
      news_detail
      title
      type
      views
    }
  }
`;

const Chess = () => {
  const { data, loading, error } = useQuery(GET_NEWS);
  if (loading) return <Spin />;
  if (error)
    return (
      <div>
        <Result
          status="500"
          title="500"
          subTitle="Sorry, Backend has fallen:((."
          extra={<Button type="primary">Back Home</Button>}
        />
      </div>
    );

  return (
    <Row>
      <Col sm={24} md={4}>
        <TypeMenu />
      </Col>
      <Col sm={24} md={16}>
        {data.vw_detail_news.map((vw_detail_news: any) => (
          <SportByType key={data.id} {...vw_detail_news} />
        ))}
      </Col>
    </Row>
  );
};

export default Chess;
