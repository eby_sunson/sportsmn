import * as React from "react";
import { Card} from "antd";
import "antd/dist/antd.css";

const { Meta } = Card;

const Recent = () => {
  return (
    <div className="recent_container">
      <Card
        className="recent_news"
        hoverable
        cover={
          <img
            alt="example"
            src="https://www.nba.com/images/cms/2020-02/walker-layup.jpg?cw=1045&w=1949&x=300&ch=588&h=1299&y=249"
          />
        }
      >
        <Meta title="Europe Street beat" description="www.instagram.com" />
      </Card>
    </div>
  );
};

export default Recent;
