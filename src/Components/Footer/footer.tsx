import * as React from "react";
import { Row, Col } from "antd";
import Icon from "@ant-design/icons";
import Logo from "../../Images/runner-logo.jpg";
import Link from "next/link";
import "../Footer/footer.css";
import Container from "../Container";

const Footer = () => {
  return (
    <Container>
      <div className="footer_wrapper">
        <Row gutter={24} style={{ marginTop: "0.1cm" }}>
          <Col xs={24} sm={6} md={6} lg={6}>
            <img src={Logo} alt="LOGO" className="footer_img" />
            <p
              style={{
                color: "#454545",
                lineHeight: 1.8,
                padding: "1rem 0",
              }}
            >
              Монголын бүх спортын мэдээ мэдээллийг нэг дор төвлөрүүлсэн
              мэдээллийн сан
            </p>
          </Col>
          <Col xs={24} sm={6} md={6} lg={6}>
            <h3>Туслах цэс</h3>
            <ul>
              <li>
                <a href="https://info.esan.mn/">Сайтын тухай</a>
              </li>

              <li>
                <Link href="/">
                  <a href="/">Үйлчилгээний нөхцөл</a>
                </Link>
              </li>
              <li>
                <a href="https://info.esan.mn/p/">Хамтран ажиллах</a>
              </li>
              <li>
                <a href="https://info.esan.mn/%d0%bd%d1%83%d1%83%d1%86%d0%bb%d0%b0%d0%bb%d1%8b%d0%bd-%d0%b1%d0%be%d0%b4%d0%bb%d0%be%d0%b3%d0%be/">
                  Нууцлалын бодлого
                </a>
              </li>
            </ul>
          </Col>
          <Col xs={24} sm={6} md={6} lg={6}>
            <h3>Тусламж</h3>
            <ul>
              <li>
                <a href="https://info.esan.mn/h/">Мэдэээ нийлүүлэгч болох</a>
              </li>
              <li>
                <a href="https://info.esan.mn/doc/">Системийг ашиглах заавар</a>
              </li>
              <li>
                <a href="https://info.esan.mn/keyword/">Нэр томъёоны тайлбар</a>
              </li>
              <li>
                <a href="https://info.esan.mn/faq/">Түгээмэл асуулт</a>
              </li>
            </ul>
          </Col>
          <Col xs={24} sm={6} md={6} lg={6}>
            <h3>Холбоо барих</h3>
            <address>
              <p>
                <strong>Хаяг:</strong>
              </p>
              <p>Хан-Уул дүүрэг, 15-р хороо,</p>
              <p>LS плаза, 10 давхар</p>
              <p>
                <strong>Утас: </strong>(+976) 96025066
              </p>
              <p>
                <strong>И-Мэйл: </strong>contact@sports.mn
              </p>
            </address>
            <a href="https://www.facebook.com/esanlibrary/">
              <Icon type="facebook" style={{ fontSize: "20px" }} />
            </a>
            <a href="https://www.instagram.com/esan.official">
              <Icon
                type="instagram"
                style={{
                  fontSize: "20px",
                  marginLeft: "20px",
                }}
              />
            </a>
          </Col>
        </Row>
        {/* <div className="copyright">
          © {new Date().getFullYear()} <a href="#">sport.mn</a> Programmer LLC.
          Зохиогчийн эрх хуулиар хамгаалагдсан.
        </div> */}
      </div>
    </Container>
  );
};

export default Footer;
