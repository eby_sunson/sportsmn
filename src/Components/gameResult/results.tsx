import * as React from "react";
import { Col, Row, Button } from "antd";

const Results = () => {
  return (
    <div>
      <Col
        sm={24}
        md={22}
        xl={22}
        style={{
          padding: "20px",
          backgroundColor: "#e8e8e8",
          justifyContent: "space-between",
          borderRadius: "20px",
          marginTop: "1cm",
        }}
      >
        <Row
          style={{
            justifyContent: "space-between",
          }}
        >
          <small>Баг</small>
          <small>Оноо</small>
        </Row>
        <hr style={{ borderTop: "1px solid black" }} />
        <Row
          style={{
            justifyContent: "space-between",
          }}
        >
          <h3>Танат гарьд</h3>
          <h3>109</h3>
        </Row>
        <Row
          style={{
            justifyContent: "space-between",
          }}
        >
          <h3>Хасын хүлгүүд</h3>
          <h3>110</h3>
        </Row>
        <hr style={{ borderTop: "1px solid black" }} />
        <Row
          style={{
            justifyContent: "center",
          }}
        >
          <h3>Амараа: 39 оноо | Сараа: 50 оноо</h3>
        </Row>
        <hr style={{ borderTop: "1px solid black" }} />
        <Row style={{ justifyContent: "center" }}>
          <Button
            type="primary"
            style={{
              backgroundColor: "#e8e8e8",
              color: "black",
              borderColor: "black",
            }}
          >
            Тоглолт
          </Button>
        </Row>
      </Col>
      <Col
        sm={24}
        md={24}
        xl={24}
        style={{
          padding: "20px",
          backgroundColor: "#e8e8e8",
          justifyContent: "space-between",
          borderRadius: "20px",
          marginTop: "1cm",
        }}
      >
        <Row
          style={{
            justifyContent: "space-between",
          }}
        >
          <small>Баг</small>
          <small>Оноо</small>
        </Row>
        <hr style={{ borderTop: "1px solid black" }} />
        <Row
          style={{
            justifyContent: "space-between",
          }}
        >
          <h3>Танат гарьд</h3>
          <h3>109</h3>
        </Row>
        <Row
          style={{
            justifyContent: "space-between",
          }}
        >
          <h3>Хасын хүлгүүд</h3>
          <h3>110</h3>
        </Row>
        <hr style={{ borderTop: "1px solid black" }} />
        <Row
          style={{
            justifyContent: "center",
          }}
        >
          <h3>Амараа: 39 оноо | Сараа: 50 оноо</h3>
        </Row>
        <hr style={{ borderTop: "1px solid black" }} />
        <Row style={{ justifyContent: "center" }}>
          <Button
            type="primary"
            style={{
              backgroundColor: "#e8e8e8",
              color: "black",
              borderColor: "black",
            }}
          >
            Тоглолт
          </Button>
        </Row>
      </Col>
    </div>
  );
};

export default Results;
