import * as React from "react";
import {
  Form,
  Select,
  InputNumber,
  Switch,
  Radio,
  Slider,
  Button,
  Upload,
  Rate,
  Checkbox,
  Row,
  Col,
  DatePicker,
  Input,
  Card,
  Spin,
  Result,
  notification,
} from "antd";
import { UploadOutlined, InboxOutlined } from "@ant-design/icons";
import Container from "../Container";
import { gql, from } from "apollo-boost";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { FormProps } from "antd/lib/form";
import { useState } from "react";

const GET_NEWS = gql`
  {
    vw_type_name {
      name
      id
      type
    }
  }
`;
const AUTHOR = gql`
  {
    authors {
      name
      id
    }
  }
`;

const insert_mutation = gql`
  mutation insertNews($data: news_insert_input!) {
    insert_news(objects: [$data]) {
      returning {
        img_url
        description
        news_detail
        title
        type
        author_id
        created_at
      }
    }
  }
`;

interface IProps extends FormProps {
  title?: string;
  type?: string;
  created_at?: Date;
  img_url?: string;
  description: string;
}

const { Option } = Select;
const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};
const { TextArea } = Input;

const Admin = () => {
  const [form] = Form.useForm();
  const { data: data2, error: error2, loading: loading2 } = useQuery(AUTHOR);
  const { data, error, loading } = useQuery(GET_NEWS);
  const [mutData] = useMutation(insert_mutation);

  if (loading) return <Spin />;
  if (error)
    return (
      <div>
        <Result
          status="500"
          title="500"
          subTitle="Sorry, Backend has fallen:((."
          extra={<Button type="primary">Back Home</Button>}
        />
        ,
      </div>
    );
  const onFinish = async (values: any) => {
    console.log(values);
    await mutData({
      variables: {
        data: {
          ...values,
          description: values.news_detail.substring(0, 150),
        },
      },
    });
    openNotification("bottomRight");
  };

  const openNotification = (placement: any) => {
    notification.info({
      message: `Мэдэгдэл `,
      description: "Мэдээ амжилттай хадгалагдлаа!",
      placement,
    });
  };

  return (
    <Container>
      <Card title="Мэдээ оруулах">
        <Form name="type" {...formItemLayout} form={form} onFinish={onFinish}>
          <Form.Item
            name="type"
            label="Төрөл"
            rules={[
              {
                required: true,
                message: "Мэдээний төрлийг сонгоно уу!",
              },
            ]}
          >
            <Select placeholder="Мэдээний төрлийг сонгоно уу!">
              {data.vw_type_name.map(({ id, type }: any) => (
                <Option key={id} value={type}>
                  {type}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name="title"
            label="Гарчиг"
            hasFeedback
            rules={[{ required: true, message: "Гарчигаа оруулана уу!" }]}
          >
            <Input placeholder="Гарчиг" />
          </Form.Item>
          <Form.Item
            name="author_id"
            label="Мэдээний ажилтан"
            rules={[
              {
                required: true,
                message: "Мэдээний ажилтныг сонгоно уу!",
              },
            ]}
          >
            <Select placeholder="Мэдээний ажилтныг сонгоно уу!">
              {data.vw_type_name.map(({ id, name }: any) => (
                <Option key={id} value={id}>
                  {name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name="date"
            label="Огноо"
            hasFeedback
            rules={[{ required: true, message: "Мэдээ оруулсан огноо!" }]}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item
            name="news_detail"
            label="Дэлгэрэнгүй мэдээ"
            hasFeedback
            rules={[
              { required: true, message: "Дэлгэрэнгүй мэдээ оруулана уу!" },
            ]}
          >
            <TextArea placeholder="Дэлгэрэнгүй мэдээ" />
          </Form.Item>

          <Form.Item
            name="img_url"
            label="Зурагны линк"
            hasFeedback
            rules={[{ required: true, message: "Зурагны линк оруулана уу!" }]}
          >
            <Input placeholder="Зурагны линк" />
          </Form.Item>

          <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
            <Button type="primary" htmlType="submit">
              <UploadOutlined /> Мэдээ оруулах
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Container>
  );
};
export default Admin;
