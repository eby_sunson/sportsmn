import * as React from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";
import basket from "../../Images/basket.png";
import voll from "../../Images/voll.png";
import chess from "../../Images/chess.png";
import foot from "../../Images/foot.png";
import judo from "../../Images/judo.png";
import undes from "../../Images/undes.png";
import "./style.css";

const TypeMenu = () => {
  return (
    <div style={{ marginTop: "30px" }}>
      <Menu
        // defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
      >
        <Menu.Item key="1">
          <Link
            to="/basketball"
            style={{
              padding: "10px",
            }}
          >
            <img src={basket} alt="menu_baskter" className="img" />
            <span className="span">Сагсан бөмбөг </span>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link
            to="/football"
            style={{
              padding: "10px",
            }}
          >
            <img src={foot} alt="menu_baskter" className="img" />
            <span className="span">Хөл бөмбөг </span>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link
            to="/judo"
            style={{
              padding: "10px",
            }}
          >
            <img src={judo} alt="menu_baskter" className="img" />
            <span className="span"> Жудо </span>
          </Link>
        </Menu.Item>

        <Menu.Item key="4">
          <Link
            to="/undes"
            style={{
              padding: "10px",
            }}
          >
            <img src={undes} alt="menu_baskter" className="img" />
            <span className="span">Үндэсний бөх</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="5">
          <Link
            to="/undes"
            style={{
              padding: "10px",
            }}
          >
            <img src={chess} alt="menu_baskter" className="img" />
            <span className="span"> Шатар </span>
          </Link>
        </Menu.Item>
        <Menu.Item key="6">
          <Link
            to="/volleyball"
            style={{
              padding: "10px",
            }}
          >
            <img src={voll} alt="menu_baskter" className="img" />
            <span className="span">Воллейбол </span>
          </Link>
        </Menu.Item>
      </Menu>
    </div>
  );
};

export default TypeMenu;
