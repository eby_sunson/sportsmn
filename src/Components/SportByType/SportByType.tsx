import * as React from "react";
import { Row, Col, Menu, Popover } from "antd";
import { Link } from "react-router-dom";
import {
  ArrowRightOutlined,
  FacebookFilled,
  TwitterCircleFilled,
  SmallDashOutlined,
} from "@ant-design/icons";
import "./style.css";
import Container from "../Container";
import ReactPlayer from "react-player";

interface IProps {
  type: String;
  title: String;
  name: String;
  created_at: Date;
  description: String;
  img_url: String;
}
const types = {
  basketball: "Сагсан бөмбөг",
  volleyball: "Воллейбол",
  chess: "Шатар",
  football: "Хөл бөмбөг",
  judo: "Жудо",
};

const SportByType = (props: IProps) => {
  return (
    <Container>
      <Row style={{ margin: "30px 0 30px", padding: "10px" }}>
        <Col sm={24} md={8}>
          <img
            src={`${props.img_url}`}
            style={{ borderRadius: "5px", width: "100%", height: "100%" }}
          />
        </Col>
        <Col
          sm={24}
          md={8}
          style={{ paddingLeft: "20px", paddingRight: "20px" }}
        >
          <small>
            <a className="news_a" href={`/${props.type}`}>
              {types[props.type as keyof typeof types]}
            </a>
          </small>
          <a style={{ color: "black", marginTop: "10px" }}>
            <h3> {props.title} </h3>
          </a>
          <small>{props.name}</small>
          <small style={{ marginLeft: "20px" }}> {props.created_at}</small>
          <p>
            {props.description}
            <a style={{ color: "black", position: "absolute" }}>
              <Popover content="Дэлгэрэнгүй...">
                <SmallDashOutlined
                  style={{ color: "black", position: "relative" }}
                />
              </Popover>
            </a>
          </p>
          <div
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <span className="news_a">SHARE</span>
            <ArrowRightOutlined
              className="news_a"
              style={{
                margin: "0px 10px 0px 10px",
              }}
            />
            <a href="https://www.facebook.com/">
              <FacebookFilled
                className="news_a"
                style={{
                  // margin: "0px 10px 0px 10px",
                  color: "black",
                }}
              />
            </a>
            <a href="https://twitter.com/explore">
              <TwitterCircleFilled
                className="news_a"
                style={{
                  margin: "0px 10px 0px 10px",
                  color: "black",
                }}
              />
            </a>
          </div>
        </Col>
      </Row>
    </Container>
  );
};
export default SportByType;
