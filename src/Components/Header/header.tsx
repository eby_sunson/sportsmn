import * as React from "react";
import "antd/dist/antd.css";
import "./header.css";
import { Menu, Row, Col, Input } from "antd";
import { Link } from "react-router-dom";
import Icon, {
  MenuOutlined,
  UserOutlined,
  SearchOutlined,
  MoreOutlined,
} from "@ant-design/icons";
import HeaderUp from "./haederUp";
import useMedia from "use-media";
import MenuItem from "antd/lib/menu/MenuItem";
import Container from "../Container";
const { SubMenu } = Menu;
const { Search } = Input;

const Header = () => {
  const isTablet = useMedia({ maxWidth: 1023 });

  if (isTablet) {
    return (
      <>
        <Row>
          <Col md={4}>
            <MenuOutlined />
          </Col>
          <Col md={16}>
            <div style={{ justifyContent: "center", textAlign: "center" }}>
              СПОРТ.МН
            </div>
          </Col>
          <Col md={4}>
            <UserOutlined />
          </Col>
        </Row>
      </>
    );
  }
  return (
    <Container>
      <div className="header_container" style={{ display: "flex" }}>
        <Row>
          <HeaderUp />
        </Row>
        <Row>
          <Col md={18}>
            <Menu
              mode="horizontal"
              className="menu"
              defaultSelectedKeys={["home"]}
              style={{
                fontSize: "0.8125rem",
                fontWeight: "bold",
                letterSpacing: "0.025em",
                flexDirection: "column",
                color: "black",
              }}
            >
              <Menu.Item key="home">
                <Link to="/" className="link">
                  Нүүр
                </Link>
              </Menu.Item>
              <Menu.Item key="basketball">
                <Link to="/basketball">Сагсан бөмбөг</Link>
              </Menu.Item>
              <Menu.Item key="football">
                <Link to="/football">Хөл бөмбөг</Link>
              </Menu.Item>
              <Menu.Item key="judo">
                <Link to="/judo">Жудо</Link>
              </Menu.Item>
              <Menu.Item key="u_boh">
                <Link to="/undes">Үндэсний бөх</Link>
              </Menu.Item>
              <SubMenu
                key="other"
                title={
                  <span style={{ alignItems: "center", fontWeight: 200 }}>
                    <MoreOutlined style={{ alignItems: "center" }} />
                    Бусад
                  </span>
                }
              >
                <Menu.ItemGroup>
                  <Menu.Item key="chess">
                    <Link to="/index">Шатар</Link>
                  </Menu.Item>
                  <Menu.Item key="setting:2">Жудо</Menu.Item>
                </Menu.ItemGroup>
              </SubMenu>
            </Menu>
          </Col>
          <Col md={6}>
            <Search
              placeholder="Хайх"
              onSearch={(value) => console.log(value)}
              style={{
                width: "100%",
                marginTop: "0.5rem",
                borderRadius: "20px",
                borderColor:"black"
              }}
            />
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default Header;
