import * as React from "react";
import { Link, Route } from "react-router-dom";
import {
  FacebookFilled,
  InstagramFilled,
  TwitterCircleFilled,
  YoutubeFilled,
  UserOutlined,
  MenuOutlined,
} from "@ant-design/icons";
import { Row, Col, Button, Menu } from "antd";
import "./headerUp.css";
import { useMedia } from "use-media";
import Container from "../Container";
import Admin from "../Admin/admin";

const headerUp = () => {
  return (
    <Container>
      <Row>
        <Col
          xs={24}
          sm={8}
          md={4}
          lg={8}
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          {/* <div className="responsive1"> */}
          <a href="https://www.facebook.com/enkhbayar.narankhuu.3">
            <FacebookFilled />
          </a>
          <a href="https://www.instagram.com/enarankhuu/">
            <InstagramFilled />
          </a>
          <a href="https://twitter.com/explore">
            <TwitterCircleFilled />
          </a>
          <a href="https://www.youtube.com/channel/UCSpeyj2_6L2nDTIXZxH7dNg?view_as=subscriber">
            <YoutubeFilled />
          </a>
          {/* </div> */}
        </Col>

        <Col
          xs={24}
          sm={8}
          md={16}
          lg={8}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Link to="/" style={{ fontSize: "32px", fontWeight: 600 }}>
            СПОРТ.МН
          </Link>
        </Col>
        <Col
          xs={24}
          sm={8}
          md={4}
          lg={8}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
          }}
        >
          <Link to="/admin">
            <Button
              type="primary"
              style={{
                backgroundColor: "white",
                color: "black",
                borderColor: "black",
              }}
            >
              Тоглолт
            </Button>
          </Link>
        </Col>
      </Row>
    </Container>
  );
};

export default headerUp;
