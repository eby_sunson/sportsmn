import * as React from "react";
import { Row, Col, Popover, Button, Rate, Carousel, Card } from "antd";
import {
  ArrowRightOutlined,
  FacebookFilled,
  TwitterCircleFilled,
  SmallDashOutlined,
  EyeFilled,
  InstagramFilled,
  MailFilled,
} from "@ant-design/icons";
import Container from "../Container";
import "./style.css";
import suggestNews from "../SuggestNews/suggestNews";
import Meta from "antd/lib/card/Meta";

interface IProps {
  type: String;
  title: String;
  name: String;
  created_at: Date;
  description: String;
  img_url: String;
  news_detail: String;
  views: Number;
}
const types = {
  basketball: "Сагсан бөмбөг",
  volleyball: "Воллейбол",
  chess: "Шатар",
  football: "Хөл бөмбөг",
  judo: "Жудо",
};
const desc = ["Маш муу", "Муу", "Хэвийн", "Сайн", "Маш сайн"];

const NewsDetail = (props: IProps) => {
  const [value, setValue] = React.useState(2);
  const handleChange = () => {
    setValue(value);
  };

  return (
    <Container>
      <Row style={{ marginTop: "1cm" }}>
        <Col sm={24} md={16} xl={16}>
          <Row>
            <a
              style={{
                fontWeight: 100,
                textTransform: "uppercase",
                fontSize: "11px",
              }}
              className="genre"
              href={`/${props.type}`}
            >
              {types[props.type as keyof typeof types]}
            </a>
          </Row>
          <Row>
            <p
              className="ex"
              style={{
                fontSize: "40px",
                textTransform: "capitalize",
                fontFamily: "'Source Sans Pro', sans-serif",
                color: "black",
              }}
            >
              {props.title}
            </p>
          </Row>

          <Row>
            <img
              src={`${props.img_url}`}
              style={{ borderRadius: "5px", width: "100%" }}
            />
          </Row>
          <Row style={{ marginTop: "1cm" }}>
            <p
              style={{
                textIndent: "50px",
                textAlign: "justify",
              }}
            >
              {props.news_detail}
            </p>
          </Row>
          <Row
            style={{
              alignItems: "center",
              justifyContent: "space-between",
              marginTop: "1cm",
            }}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  backgroundColor: "#e8e8e8",
                  width: "100%",
                  height: "100%",
                  borderRadius: "5px",
                  border: "1px solid black",
                  color: "black",
                  padding: "4px 15px 4px 15px  ",
                }}
              >
                <EyeFilled style={{ marginLeft: "5px" }} />
                {props.views}
              </div>
              <ArrowRightOutlined
                className="news_a"
                style={{
                  margin: "0px 10px 0px 10px",
                  color: "black",
                  fontWeight: "bold",
                }}
              />
              <Button
                type="primary"
                style={{
                  backgroundColor: "#e8e8e8",
                  color: "black",
                  borderColor: "black",
                }}
              >
                <FacebookFilled
                  className="news_a"
                  style={{
                    // margin: "0px 10px 0px 10px",
                    color: "black",
                  }}
                />
                Хуваалцах
              </Button>
              <ArrowRightOutlined
                className="news_a"
                style={{
                  margin: "0px 10px 0px 10px",
                  color: "black",
                  fontWeight: "bold",
                }}
              />
              <Button
                type="primary"
                style={{
                  backgroundColor: "#e8e8e8",
                  color: "black",
                  borderColor: "black",
                }}
              >
                <TwitterCircleFilled
                  className="news_a"
                  style={{
                    margin: "0px 10px 0px 10px",
                    color: "black",
                  }}
                />
                Жиргэх
              </Button>
            </div>
            <span>
              <Rate tooltips={desc} onChange={handleChange} value={value} />
              {value ? (
                <span className="ant-rate-text">{desc[value - 1]}</span>
              ) : (
                ""
              )}
            </span>
          </Row>

          <Row style={{ justifyContent: "center", marginTop: "1cm" }}>
            <img
              src="https://secure.gravatar.com/avatar/77df9ef82f62f8c10b75524bfb9222ae?s=120&d=mm&r=g"
              style={{ borderRadius: "90px", width: "100px", height: "100px" }}
            />
          </Row>
          <Row style={{ justifyContent: "center", marginTop: "1cm" }}>
            <Row> {props.name} </Row>
          </Row>
          <Row style={{ justifyContent: "space-between" }}>
            <h3>
              Холбоо барих: <FacebookFilled /> <InstagramFilled />{" "}
              <MailFilled />
            </h3>
            <h3>Утас: 90919096</h3>
          </Row>
          <hr style={{ margin: "1cm 0 cm" }} />
          <Row>
            <h3>Таны сонирхож болох мэдээ</h3>
          </Row>
          <Row style={{ justifyContent: "space-between" }}>
            <Col sm={24} md={7}>
              <Card
                hoverable
                style={{ width: "100%", height: "100%" }}
                cover={
                  <img
                    alt="example"
                    src="https://authentictheme.com/heartbeat/wp-content/uploads/sites/3/2017/10/authentic-demo-image-00028-800x1200.jpg"
                  />
                }
              >
                <Meta title="Europe Street beat" description={props.type} />
              </Card>
            </Col>

            <Col sm={24} md={7}>
              <Card
                hoverable
                style={{ width: "100%", height: "100%" }}
                cover={
                  <img
                    alt="example"
                    src="https://authentictheme.com/heartbeat/wp-content/uploads/sites/3/2017/10/authentic-demo-image-00029-800x533.jpg"
                  />
                }
              >
                <Meta title="Europe Street beat" description={props.type} />
              </Card>
            </Col>
            <Col sm={24} md={7}>
              <Card
                hoverable
                style={{ width: "100%", height: "100%" }}
                cover={
                  <img
                    alt="example"
                    src="https://authentictheme.com/authentic/wp-content/uploads/sites/2/2017/10/authentic-demo-image-00020-800x800.jpg"
                  />
                }
              >
                <Meta title="Europe Street beat" description={props.type} />
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default NewsDetail;
