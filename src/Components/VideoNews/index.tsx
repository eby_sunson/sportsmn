import * as React from "react";
import { Row, Col } from "antd";
import SublimeVideo from "react-sublime-video";

const VideoNews = () => {
  return (
    <Row>
      <Col sm={24} md={24} lg={24}>
        <SublimeVideo
          autoPlay
          loop
          style={{}}
          source="https://cdn.sublimevideo.net/vpa/ms_720p.webm"
        />
      </Col>
    </Row>
  );
};

export default VideoNews;
