import React from "react";
import ReactDOM from "react-dom";
import Routes from "./routes";
import "./index.css";

const Index = () => {
  return (
    <div>
      <Routes />
    </div>
  );
};

ReactDOM.render(<Index />, document.getElementById("root"));
